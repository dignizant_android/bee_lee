package com.beelee;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.beelee.Interface.ApiInterface;
import com.beelee.constants.Api;
import com.beelee.constants.Constants;
import com.beelee.model.DataCountryList;
import com.beelee.model.GsonCounrtyList;
import com.beelee.model.GsonRegister;
import com.beelee.utility.LogUtil;
import com.beelee.utility.MyUtils;
import com.beelee.utility.NavigatorManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Register extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "Register";
    public static List<DataCountryList> list_country = new ArrayList<>();
    TextView tv_all_acc;
    ImageView iv_back;
    EditText et_name, et_email, et_number;
    TextView et_pin_code;
    TextView et_dob;
    Spinner sp_country;
    ProgressBar cpb;
    ArrayAdapter<String> adp_country;

    /*date picker*/
    String selected_date = "", today_date = "";
    String s_birth_date;
    Button btn_verify;
    String countryname = null;
    private Calendar calendar;
    private int year, month, day;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.RegisterActivityForBugsense(Register.this);
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            window.setStatusBarColor(Color.TRANSPARENT);
//            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        setContentView(R.layout.activity_register);


        countryname = getIntent().getExtras().getString(Constants.country);


        findViewById();
        main();

        if (list_country.size() == 0)
            getCountryListAPIRequest();
        else
            FillCountrySpinnerData();
    }


    private void findViewById() {
        cpb = findViewById(R.id.cpb);
        tv_all_acc = findViewById(R.id.tv_all_acc);
        iv_back = findViewById(R.id.iv_back);

        et_name = findViewById(R.id.et_name);
        et_email = findViewById(R.id.et_email);
        sp_country = findViewById(R.id.sp_country);
        et_pin_code = findViewById(R.id.et_pin_code);
        et_number = findViewById(R.id.et_number);
        et_dob = findViewById(R.id.et_dob);
        btn_verify = findViewById(R.id.btn_verify);

    }

    private void main() {
        tv_all_acc.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        et_dob.setOnClickListener(this);
        btn_verify.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        if (view == iv_back) {
            onBackPressed();
            finish();
        }
        if (view == et_dob) {
            setBirthDateDialog();
        }

        if (view == tv_all_acc) {
            NavigatorManager.startNewActivity(Register.this, new Intent(Register.this, Login.class));
            finish();
        }
        if (view == btn_verify) {

//            if (et_name.getText().toString().isEmpty()) {
//                et_name.requestFocus();
//                MyUtils.makeSnackbar(et_name, getResources().getString(R.string.text_err_name));
//            } else if (et_email.getText().toString().isEmpty()) {
//                et_email.requestFocus();
//                MyUtils.makeSnackbar(et_email, getResources().getString(R.string.text_err_email));
//            } else if (!App.Utils.IsValidEmail(et_email.getText().toString())) {
//                et_email.requestFocus();
//                MyUtils.makeSnackbar(et_email, getResources().getString(R.string.text_err_invalid_email));
//            } else if (sp_country.getSelectedItemPosition() == 0) {
//                sp_country.requestFocus();
//                App.Utils.makeSnackbar(sp_country, getResources().getString(R.string.text_err_country));
//            }
//            else
            if (et_number.getText().toString().isEmpty()) {
                et_number.requestFocus();
                MyUtils.makeSnackbar(et_number, getResources().getString(R.string.text_err_mobile));
            }
//            else if (et_dob.getText().toString().isEmpty()) {
//                et_dob.requestFocus();
//                MyUtils.makeSnackbar(et_dob, getResources().getString(R.string.text_err_birthday));
//            }
            else {
                // MyUtils.makeToast("Success");
                CallRegisterAPI();
            }

        }

    }


    private void setBirthDateDialog() {
        calendar = Calendar.getInstance(Locale.ENGLISH);
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        if (!selected_date.equals("")) {
            try {
                Date date = new SimpleDateFormat(Constants.DATE_YYYY_MM_DD_FORMAT)
                        .parse(selected_date);
                long timeInMillis = date.getTime();
                Calendar calendar = Calendar.getInstance(Locale.ENGLISH);
                calendar.setTimeInMillis(timeInMillis);
                year = calendar.get(Calendar.YEAR);
                month = calendar.get(Calendar.MONTH);
                day = calendar.get(Calendar.DAY_OF_MONTH);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        createDialogForFromDate().show();
    }

    private DatePickerDialog createDialogForFromDate() {
        DatePickerDialog dpd = new DatePickerDialog(this, R.style.DialogThemeCalendar,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        String date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        LogUtil.Print(TAG, date);
                        try {

                            selected_date = App.Utils.GetDateOnRequireFormat(date, Constants.DATE_YYYY_MM_DD_FORMAT, Constants.DATE_YYYY_MM_DD_FORMAT);
                            s_birth_date = selected_date;
                            System.out.println(selected_date);
                            et_dob.setText(s_birth_date);

                        } catch (Exception e) {
                            // TODO: handle exception
                            e.printStackTrace();
                        }
                    }
                }, year, (month), day);
        try {
            Date selectedDate_max = new SimpleDateFormat(Constants.DATE_DD_MM_YYYY_FORMAT)
                    .parse(today_date);
            dpd.getDatePicker().setMaxDate(selectedDate_max.getTime());

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return dpd;
    }

    private void getCountryListAPIRequest() {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();


            params.put(Constants.lang, Constants.language_en);

            LogUtil.Print("Country_params", "" + params);

            Call<GsonCounrtyList> call = request.getCountryList(params);
            cpb.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<GsonCounrtyList>() {
                @Override
                public void onResponse(Call<GsonCounrtyList> call, Response<GsonCounrtyList> response) {
                    cpb.setVisibility(View.GONE);
                    GsonCounrtyList gson = response.body();

                    if (gson.getFlag().equals(Constants.RESPONSE_SUCCESS_FLAG)) {
                        list_country.clear();
                        list_country.addAll((gson.getData()));
                        // App.Utils.putString(Constants.community_id, gson.getData().getCommunityId());
                        FillCountrySpinnerData();
                    } else {

                        App.Utils.makeSnackbar(sp_country, gson.getMsg());

                    }
                }

                @Override
                public void onFailure(Call<GsonCounrtyList> call, Throwable t) {
                    Log.d("Error", t.getMessage());
                }
            });

        } else {
            App.Utils.makeSnackbar(sp_country, getResources().getString(R.string.text_internet));

        }

    }

    private void FillCountrySpinnerData() {
        final List<String> temp_list = new ArrayList<>();
        temp_list.add("" + getResources().getString(R.string.text_Select_Country));
        for (int i = 0; i < list_country.size(); i++) {
            temp_list.add("" + list_country.get(i).getCountry_name());
        }
        if (countryname.equals("notget")) {
            adp_country = new ArrayAdapter<String>(Register.this, R.layout.lv_spinner, R.id.tv_spn, temp_list);
            adp_country.setDropDownViewResource(R.layout.lv_spinner);
            sp_country.setAdapter(adp_country);

        } else {
            adp_country = new ArrayAdapter<String>(Register.this, R.layout.lv_spinner, R.id.tv_spn, temp_list);
            adp_country.setDropDownViewResource(R.layout.lv_spinner);
            sp_country.setAdapter(adp_country);
            int sp = adp_country.getPosition(getIntent().getExtras().getString(Constants.country));
            sp_country.setSelection(sp);

        }


        sp_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                last_selected_block = position;
//                LogUtil.Print(TAG, "last_selected_block --> " + last_selected_block);
             /*   if (last_selected_block != 0) {
                    list_flat.clear();
                    getFlatListAPIRequest();
                } else {
                    list_flat.clear();
                    FillFlatSpinnerData();
                }*/
                if (position != 0)
                    et_pin_code.setText(list_country.get(position - 1).getCountry_code());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void CallRegisterAPI() {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.lang, Constants.language_en);
            params.put(Constants.device_token, App.Utils.GetDeviceID());
            params.put(Constants.register_id, App.Utils.getString(Constants.device_token));
            params.put(Constants.device_type, Constants.DEVICE_TYPE_ANDROID);
            params.put(Constants.mobile_no, et_number.getText().toString());
            if (et_name.getText().toString().isEmpty()) {
                params.put(Constants.username, "");
            } else {
                params.put(Constants.username, et_name.getText().toString());
            }
            if (et_email.getText().toString().isEmpty()) {
                params.put(Constants.email, "");
            } else {
                params.put(Constants.email, et_email.getText().toString());
            }

            if (et_pin_code.getText().toString().isEmpty()) {
                params.put(Constants.country_code, "");
            } else {
                params.put(Constants.country_code, et_pin_code.getText().toString());
            }

            if (sp_country.getSelectedItemPosition() != 0) {
                params.put(Constants.country_id, list_country.get(sp_country.getSelectedItemPosition() - 1).getCountry_id());
            } else {
                params.put(Constants.country_id, "0");
            }
            if (et_dob.getText().toString().isEmpty()) {
                params.put(Constants.birthdate, "");
            } else {
                params.put(Constants.birthdate, et_dob.getText().toString());
            }


            LogUtil.Print("Register_params", "" + params);


            Call<GsonRegister> call = request.getUserRegister(params);
            cpb.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<GsonRegister>() {
                @Override
                public void onResponse(Call<GsonRegister> call, Response<GsonRegister> response) {
                    cpb.setVisibility(View.GONE);
                    GsonRegister gson = response.body();

                    if (gson.getFlag().equals(Constants.RESPONSE_SUCCESS_FLAG)) {

//                        App.Utils.makeSnackbar(et_name, gson.getMsg());
//                        App.Utils.putString(Constants.user_id, gson.getData().getUser_id());
//                        App.Utils.putString(Constants.access_token, gson.getData().getAccess_token());
//                        App.Utils.putString(Constants.mobile_no, gson.getData().getMobile_no());
//                            App.Utils.putString(Constants.user_id, gson.getData().getId());
//                            App.Utils.putString(Constants.access_token, gson.getData().getAccessToken());
                        //  App.Utils.putString(Constants.community_id, gson.getData().getCommunityId());
//                            if (gson.getData().getUserType().equals(Constants.Default)) {
                        LogUtil.Print("register: - ", gson.getData().getUser_id());
                        Intent i = new Intent(Register.this, Verify_otp.class);
                        i.putExtra(Constants.user_id, gson.getData().getUser_id());
                        i.putExtra(Constants.access_token, gson.getData().getAccess_token());
                        i.putExtra(Constants.mobile_no, gson.getData().getMobile_no());
                        NavigatorManager.startNewActivity(Register.this, i);
                        finish();

//                            }
                    } else if (gson.getFlag().equals(Constants.RESPONSE_UNSUCCESS_FLAG)) {
                        App.Utils.makeSnackbar(et_dob, gson.getMsg());
                    }
                }

                @Override
                public void onFailure(Call<GsonRegister> call, Throwable t) {
                    Log.d("Error", t.getMessage());

                }
            });

        } else {
            App.Utils.makeSnackbar(et_dob, getResources().getString(R.string.text_internet));

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
