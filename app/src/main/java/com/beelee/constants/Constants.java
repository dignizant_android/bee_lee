package com.beelee.constants;

import android.os.Environment;

/**
 * Created by chandresh on 07/25/2016.
 */
public class Constants {
    /*
    * Preference name
    * */

    public static final String APP_PdfKG = "com.beelee";
    public static String gcm_registration_id = "gcm_registration_id";

    public static String transaction_id = "transaction_id";
    public static String payment_type = "payment_type";
    public static String paypal = "1";
    public static String credit = "2";
    public static String other = "3";


    public static String minQty = "0";
    public static String maxQty = "100";
    public static int homeDetail = 1;
    public static int homePay = 2;



    public static int ITEM_CLICK = 10;
    public static int ITEM_PLUS = 30;
    public static int ITEM_MINUS = 40;
    public static int ITEM_NOTES = 50;
    public static int ITEM_QTY = 60;

    public static String product_id = "product_id";
    public static String quantity = "quantity";
    public static String notes = "notes";


    public static String data = "data";
    public static String key = "key";
    public static String total = "total";
    public static String flag_view = "flag_view";
    public static String detail = "detail";
    public static String is_flag = "is_flag";

    public static String come_from = "come_from";
    public static String send_from_user = "send_from_user";
    public static String device_id = "device_id";
    public static String onServerExpirationTimeMs = "onServerExpirationTimeMs";
    public static String appVersion = "appVersion";
    public static String sender_id = "1012077150789";
    public static String push_message = "push_message";
    public static String msg = "message";
    public static String book_date_from = "book_date_from";
    public static String number_of_day = "number_of_day";
    public static String msg_ar = "message_ar";
    public static String dataPrimary = "dataPrimary";
    public static String dataSecondary = "dataSecondary";
    public static int ITEM_EDIT = 20;
    public static int ITEM_DELETE = 30;
    public static int ITEM_MENU = 110;
    public static int ITEM_FLAT = 20;
    public static int ITEM_FLOR= 10;
    public static int TOGGLE_CLICK = 10;
    public static String DEVICE_TYPE_ANDROID = "0";
    public static String push_type_without = "100";
    public static String report_msg = "report_msg";
    public static String reservation_type = "reservation_type";
    public static int ITEM_REMOVECART = 70;

    public static String Content_type_json = "application/json";
    public static String Content_type_multipart = "multipart/form-data";
    public static final String position = "position";
    public static final String CategoryId = "CategoryId";

    public static final String from = "from";
    public static final int FROM_INQUIRY = 0;
    public static final int FROM_RESERVATION = 1;
    public static final int Home = 10;

    public static final String FROM_UPDATE_PASSWORD = "1";
    public static final String FROM_OTHER = "0";

    public static final float MAP_DEFAULT_ZOOM_LEVEL = 12.0f;
    public static final int REQUEST_CODE_GPS_ON = 111;
    public static final int REQUEST_CODE_CATEGORY_FILTER = 112;
    public static final int REQUEST_CODE_COMPANY_CATEGORY_FILTER = 113;

    public static final int REQUEST_CODE_STORAGE_PERMISSION = 114;
    public static final int REQUEST_CODE_CALL_PERMISSION = 115;
    public static final int REQUEST_CODE_GPS_PERMISSION = 116;


    public static final int RESPONSE_SUCCESS_FLAG = 1;
    public static final int RESPONSE_UNSUCCESS_FLAG = 0;

    public static String image_crop = "image_crop";


    public static final int DEFAULT_REGISTER_PICTURE_CROP_RATIO_WIDTH = 1;
    public static final int DEFAULT_REGISTER_PICTURE_CROP_RATIO_HEIGHT = 1;

    public static final int DEFAULT_PICTURE_CROP_RATIO_WIDTH = 1;
    public static final int DEFAULT_PICTURE_CROP_RATIO_HEIGHT = 1;
    public static final String IMAGE_CROP_RATIO_WIDTH = "image_crop_ratio_width";
    public static final String IMAGE_CROP_RATIO_HEIGHT = "image_crop_ratio_height";


    /*push*/
    public static final String custom_data = "custom_data";
    public static final String notifyID = "notifyID";


    /*
    * Login and Register  Constant
    * */

    public static String username = "username";
    public static String email = "email";
    public static String mobile_no = "mobile_no";
    public static String country_id = "country_id";
    public static String otp_code = "otp_code";
    public static String qrcode = "qrcode";
    public static String product_code = "product_code";
    public static String seller_id = "seller_id";


    public static String twitter_id = "twitter_id";
    public static String property_name = "property_name";
    public static String amenities_ids = "amenities_ids";
    public static String pay_at_hotel = "pay_at_hotel";
    public static String pay_to_bank_account = "pay_to_bank_account";
    public static String account_number = "account_number";
    public static String property_address = "property_address";
    public static String property_latitude = "property_latitude";
    public static String property_longitude = "property_longitude";
    public static String property_unit = "property_unit";
    public static String landline_number = "landline_number";
    public static String sms_number = "sms_number";
    public static String whatsapp_number = "whatsapp_number";
    public static String whatsapp = "whatsapp";
    public static String twitter_account = "twitter_account";
    public static String about_property = "about_property";
    public static String unit_id = "unit_id";

    public static String is_login = "is_login";
    public static String unit = "unit";
    public static String unit_price = "unit_price";
    public static String unit_currency = "unit_currency";
    public static String availability = "availability";
    public static String discount = "discount";
    public static String property_image_id = "property_image_id";
    public static String PROPERTY_TYPE_SINGLE = "1";
    public static String PROPERTY_TYPE_FAMILY = "2";
    public static String PROPERTY_TYPE_BOTH = "0";
    public static String UNIT_AVAILABLE = "1";
    public static String property_id = "property_id";
    public static String review = "review";
    public static String rate_star = "rate_star";
    public static String property_photo = "property_photo";
    public static String is_del = "is_del";
    public static String is_edit = "is_edit";
    public static String LOCAL_IMAGE = "2";
    public static String SERVER_IMAGE = "1";
    public static String IS_DEL = "1";
    public static String is_from_server = "is_from_server";
    public static String img_name = "name";
    public static String img_id = "img_id";
    public static String latitude = "latitude";
    public static String longitude = "longitude";
    public static String APP_MODE = "APP_MODE";
    public static String APP_MODE_USER = "1";
    public static String APP_MODE_AGENT = "2";
    public static String user_gson = "user_gson";
    public static String user_id = "user_id";
    public static String community_id = "community_id";
    public static String default_community_id = "15";
    public static String block_id = "block_id";

    public static String flat_floor = "flat_floor";
    public static String access_token = "access_token";
    public static String is_user_verified = "is_user_verified";

    public static String lang = "lang";

    public static String user_type = "user_type";
    public static String city_id = "city_id";
    public static String activation_code = "activation_code";
    public static String timezone = "timezone";
    public static String send_from = "send_from";
    public static String user = "user";

    public static String device_token = "device_token";
    public static String register_id = "register_id";
    public static String device_type = "device_type";
    public static String district_id = "district_id";

    public static String gender = "gender";
    public static String MALE = "1";
    public static String FEMALE = "0";
    public static String mobile_number = "mobile_number";
    public static String agent_email = "agent_email";
    public static String agent_name = "agent_name";
    public static String agent_latitude = "agent_latitude";
    public static String agent_longitude = "agent_longitude";
    public static String agent_location = "agent_location";
    public static String agent_type = "agent_type";
    public static String licence_image = "licence_image";
    public static String country_code = "country_code";
    public static String birthdate = "birthdate";
    public static String position_id = "position_id";
    public static String profile_image = "profile_image";
    public static String current_password = "current_password";
    public static String new_password = "new_password";
    public static String login_id = "login_id";
    public static String agent_id = "agent_id";
    public static String conversation_id = "conversation_id";
    public static String tournament_id = "tournament_id";
    public static String match_date = "match_date";
    public static String match_time = "match_time";
    public static String offset = "offset";
    public static String search_property_name = "search_property_name";
    public static String badroom_type = "badroom_type";
    public static String property_type = "property_type";
    public static String start_price = "start_price";
    public static String end_price = "end_price";
    public static String area_id = "area_id";
    public static String inquiry_type = "inquiry_type";
    public static String sort_type = "sort_type";
    public static String is_facebook_linked = "is_facebook_linked";
    public static String is_twitter_linked = "is_twitter_linked";
    public static String IS_AVAILABLE = "1";
    public static String UNIT_UN_AVAILABLE = "0";
    public static String IS_MALE = "1";
    public static String opp_user_id = "opp_user_id";
    public static String user_ids = "user_ids";
    public static String search_text = "search_text";
    public static String is_invite_for_match = "is_invite_for_match";
    public static String is_invite_for_match_from_upcoming = "is_invite_for_match_from_upcoming";
    public static String is_joined_tournament = "is_joined_tournament";
    public static String timeslot_id = "timeslot_id";
    public static String payment_mode = "payment_mode";
    public static String match_id = "match_id";
    public static String field_id = "field_id";
    public static String for_invite_match = "for_invite_match";
    public static String lat = "lat";
    public static String lng = "lng";
    public static String title = "title";
    public static String upcoming_or_past = "upcoming_or_past";
    public static String UPCOMING = "1";
    public static String IS_JOIED_MATCCH = "1";
    public static String IS_JOINED_TOURNAMENT = "1";
    public static String PAST = "0";
    public static String invite_type = "invite_type";
    public static String activity_indicator_id = "activity_indicator_id";
    public static String action_type = "action_type";
    public static String PAY_MODE_CASH = "0";
    public static String PAY_MODE_KNET = "1";
    public static String state = "state";
    public static String postal_code = "postal_code";
    public static String country = "country";
    public static String addressline_1 = "addressline_1";


    public static String ACTIVITY_FRIEND_REQUEST_RECEIVE = "0";
    public static String ACTIVITY_MATCH_REQUEST_RECEIVE = "2";
    public static String ACTIVITY_FRIEND_REQUEST_ACCEPTED = "5";
    public static String ACTIVITY_FRIEND_REQUEST_REJECTED = "6";
    public static String ACTIVITY_MATCH_REQUEST_ACCEPTED = "7";
    public static String ACTIVITY_MATCH_REQUEST_REJECTED = "8";
    public static String ACTIVITY_TOURNAMENT_REQUEST_ACCEPTED = "11";
    public static String ACTIVITY_TOURNAMENT_REQUEST_REJECTED = "12";
    public static String ACTIVITY_TOURNAMENT_REQUEST_RECEIVE = "10";
    public static String ACTIVITY_TOURNAMENT_WITHDRAW = "13";


    public static final String DATE_SOURCE_FORMAT = "yyyy-MM-dd";
    public static final String DATE_REQUIRE_FORMAT = "dd/MM/yyyy";
    public static final String TIME_SOURCE_FORMAT = "HH:mm:ss";
    public static final String TIME_REQUIRE_FORMAT = "hh:mm a";

    public static final String DATE_DDMMYYYY_FORMAT = "dd.MM.yyyy";
    public static final String DATE_DD_MM_YYYY_FORMAT = "dd-MM-yyyy";
    public static final String DATE_DD_MM_YYYY_CFM_FORMAT = "dd/MM/yyyy";
    public static final String DATE_DD_MM_YYYY_FORMAT_DOT = "dd.M.yyyy";
    public static final String DATE_DD_MM_YYYY_HH_MM_SS_FORMAT = "dd-MM-yyyy HH:mm:ss";
    public static final String DATE_FULL_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static final String DATE_YYYY_MM_DD_FORMAT = "yyyy-MM-dd";
    public static final String DATE_TODAY_TITLE_FORMAT = "dd MMM";
    public static final String DATE_TITLE_FORMAT = "EEE - dd MMM";
    public static final String TIME_FORMAT = "HH:mm:ss";

    public static final String DATE_YYYY = "yyyy";

    public static final String TIME_HH_MM = "hh:mma";

    public static final String TIME_HH_MM_SS = "HH:mm:ss";


    public static final String Mint_key = "550d0b43";


    public static final String IS_VERIFIED = "1";

    public static String change_flag = "change_flag";


    //images

    public static final String IMAGE_FILE_NAME_PREFIX = "IMG_CAM" + "_X" + ".jpg";


    public static final String App_ROOT_FOLDER = Environment
            .getExternalStorageDirectory()
            .getAbsolutePath() + "/" + ".CAM";

    public static int FLAG_LAUNCH_CAMERA = 0;


    //API authentication

    public static String AUTH_USERNAME = "beelee";
    public static String AUTH_PASSWORD = "f&2HzXM}GUbT#&^n";


    public static String language = "language";
    public static String language_en = "0";
    public static String language_ar = "1";
    public static String IS_ENGLISH = "1";
    public static String IS_ARABIC = "0";
    public static String LANGUAGE = "LANGUAGE";


    //crop

    public static String image = "image";
    public static String FLAG_IS_SQUARE = "flag_is_square";
    public static int FLAG_CROP = 1314;


    //pagination
    public static String pagination_last_offset = "-1";
    public static String pagination_start_offset = "0";


    public static final int CART_HOME = 1;

    //select position
    public static int FLAG_EDIT_POSITION = 1315;


    //contact us
    public static String CONTACT_US_EMAIL = "info@sikkaa.com";

    //follow US
    public static String FACEBOOK_URL = "http://www.facebook.com/Sikkaa";
    public static String TWITTER_URL = "http://www.twitter.com/SikkaaApp";
    public static String INSTAGRAM_URL = "http://www.instagram.com/Sikkaaapp";


    public static String otp = "otp";
    public static String islogin = "islogin";
    public static String agent = "agent";
    public static String message = "message";
    public static String from_id = "from_id";
    public static String to_id = "to_id";
    public static String conversation_ids = "conversation_ids";
    public static String push_type = "push_type";
    public static String filterAction = "100";
    public static String push_type_inquiry = "0";
    public static String push_type_reservation = "1";
    public static int from_agent = 1;
    public static int from_user = 0;
    public static int type_map_view = 0;
    public static int type_around = 1;
    public static String send_id = "send_id";
    public static String contact_number = "contact_number";

    public static String property_type_single = "1";
    public static String property_type_family = "2";
    public static String property_type_all = "0";
    public static String badroom_type_one = "1";
    public static String badroom_type_two = "2";
    public static String badroom_type_three = "3";
    public static String send_from_user_chat = "0";
    public static String send_from_agent_chat = "1";
    public static String CurrencyName = "KWD";
    public static String CurrencyID = "1";
    public static String CurrencyResourceID = "";


    public static int ITEM_ADDCART = 20;
    public static int SELECT_CURRENCY = 60;
    public static int ITEM_ADVERTISE = 50;
    public static int ITEM_CATEGORY = 70;
    public static String FIRST_TIME = "0";
    /*category : cat=X , X is category ID , used in category pages ( playstation = 1, xbox = 2, nintendo = 3, prepaid cards = 4 )
    type : t=X , which is filter on category top bar , used in category pages ( all = 0, consoles = 1, games = 2, accessories = 3 )*/
    public static String Language = "l";
    public static String Category = "cat";
    public static String Type = "t";
    public static String Currency = "c";
    public static String Currency1 = "currency";
    public static String ProductId = "id";
    public static String token = "token";
    public static String name = "name";
    public static String model = "name";
    public static String ver = "ver";
    public static String addlink = "addlink";
    public static String address = "address";



    //    SignUp
    public static String mobile = "mobile";
    public static String password = "password";
    public static String phone_no = "phone_no";
    public static String email_id = "email_id";
    public static String ClientId = "ClientId";
    public static String date_of_birth = "date_of_birth";
    public static String user_name = "user_name";
    public static String flat_id = "flat_id";
    public static String flat_name = "flat_name";
    public static String Profile_image = "Profile_image";

    // AddNewBlock
    public static String block_name = "block_name";
    public static String num_of_floors = "num_of_floors";
    public static String num_of_flats = "num_of_flats";


    // Profile
    public static String CID = "CID";


    /*Database Table name*/
    public static String CART = "cart_product";
    public static String IMAGE_LIST = "image_list";
    public static String SELECTED_PHOTO = "selected_photo";
    /*DataBase Table Name*/
    public static String id = "id";

    public static String sub_category_id = "sub_category_id";
    public static String total_photos = "total_photos";
    public static String selected_photo_id = "selected_photo_id";
    public static String offer_price = "offer_price";
    public static String main_offer_price = "main_price";
    public static String image_text = "image_text";
    public static String image_path = "image_path";
    public static String image_path_full = "image_path_full";
    public static String cart_id = "cart_id";
    public static String offer_id = "offer_id";
    public static String compressWidth = "compressWidth";
    public static String compressHeight = "compressHeight";
    public static String compresspercentage = "compresspercentage";
    public static String pass_port_type = "pass_port_type";
    public static String pass_port_price = "pass_port_price";
    public static String pass_port_main_price = "pass_port_main_price";
    public static String pass_port_quantity = "pass_port_quantity";
    public static String image_quantity = "image_quantity";


    public static String offer_sample_image_path = "offer_sample_image_path";
    //table column
    public static String Product_name = "Product_name";
    public static String Quantity = "quantity";
    public static String virtual = "virtual";
    public static String price = "price";

    public static String IS_FROM = "IS_FROM";
    public static String IS_ADD = "0";
    public static String IS_EDIT = "1";
    public static String CHECKOUT = "CHECKOUT";
    public static String HOME = "HOME";


    public static String UserName = "UserName";
    public static String city = "city";
    public static String shipping = "shipping";
    public static String cart = "cart";
    public static String productID = "productID";
    public static String product_qty = "product_qty";
    public static String orderId = "orderId";

    public static String OrderSuccsess = "OrderSuccsess";
    public static String OrderSuccsessId = "OrderSuccsessId";
    public static String payment = "payment";


    public static String Default = "0";
    public static String Committee = "1";
    public static String Residents = "2";

    //ACTIVITY FLAGS
    public static final int REQUEST_CODE_REQUEST_DETAIL = 881;
    public static final int REQUEST_CODE_SPECIALIZATION_SELECTION = 882;
    public static final int REQUEST_CODE_SUB_SPECIALIZATION_SELECTION = 883;
    public static final int REQUEST_CODE_EDUCATION_SELECTION = 884;
    public static final int REQUEST_CODE_DAY_OF_CONSULTING = 885;
    public static final int REQUEST_CODE_PICK_FILE = 886;

    public static final int REQUEST_CODE_CAMERA = 117;


    ///////////////////////// Resident /////////////////////////////////

    public static String carpooling_details_id = "carpooling_details_id";
    public static String status = "status";
    public static String accetpreject = "accetpreject";
    public static String carpooling_user_id = "carpooling_user_id";
    public static String flag = "flag";
    public static String type = "type";
    public static String description = "description";
    public static String classified_id = "classified_id";
    public static String files = "files";
    public static String vehicle_name = "vehicle_name";
    public static String pool_date = "date";
    public static String pool_time = "time";
    public static String start_location = "start_location";
    public static String end_location = "end_location";
    public static String no_of_space = "no_of_space";
    public static String search = "search";
    public static int text_conatant = 10001;
    public static String text_delete = "delete";
    public static String image_id = "image_id";
    public static String category_id  = "category_id";
    public static String item = "item";

    public static String classifiedlist = "classifiedlist";

    //Complaints
    public static String complaintslist = "complaintslist";
    public static String complaint_id = "complaint_id";
    public static String complaint_complated = "1";


    //Event
    public static String event_title = "event_title";
    public static String from_date = "from_date";
    public static String to_date = "to_date";
    public static String event_venue = "event_venue";

    //Visitor
    public static String visitor_name = "visitor_name";
    public static String visitor_id = "visitor_id";
    public static String purpose = "purpose";
    public static String added_by = "added_by";
    public static String no_of_persons = "no_of_persons";
    public static String is_permanent = "is_permanent";
    public static String v_date = "date";
    public static String v_time = "time";







    public static String view_by = "view_by";



    ///////////////////////// Committee /////////////////////////////////
    public static String flat_number = "flat_number";
    public static String flat_type = "flat_type";
    public static String flat_area = "flat_area";
    public static String flat_status = "flat_status";
    public static String flat_facing = "flat_facing";
    public static String property_tax_detail = "property_tax_detail";
    public static String electricity_bill_detail = "electricity_bill_detail";

//Member
public static String resident_id = "resident_id";
//Member Add Primary & Secondary Resident

    public static String p_user_name = "p_user_name";
    public static String p_phone_no = "p_phone_no";
    public static String p_email_id = "p_email_id";
    public static String p_gender = "p_gender";
    public static String p_date_of_birth = "p_date_of_birth";
    public static String is_commitee  = "is_commitee";
    public static String p_profile_image  = "p_profile_image";
    public static String s_user_name  = "s_user_name";
    public static String s_phone_no  = "s_phone_no";
    public static String s_email_id  = "s_email_id";
    public static String s_gender  = "s_gender";
    public static String s_date_of_birth  = "s_date_of_birth";
    public static String s_profile_image  = "s_profile_image";
    public static String primary_user_id  = "primary_user_id";
    public static String sec_user_id  = "sec_user_id";
    public static String comment = "comment";



    ///////////////////////// Security /////////////////////////////////


    ///////////////////////// Club /////////////////////////////////



}
