package com.beelee.constants;

import com.beelee.App;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by chandresh on 07/25/2016.
 */
public class Api {

    public static String BASE_URL = "http://dignizant.com/beelee/api/";

    public static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(Api.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(App.client)
            .build();

}
