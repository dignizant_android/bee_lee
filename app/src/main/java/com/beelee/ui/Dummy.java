package com.beelee.ui;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.beelee.App;
import com.beelee.R;
import com.beelee.constants.Constants;


public class Dummy extends AppCompatActivity {
    String TAG = "Dummy";
    private static int SPLASH_WAIT = 3000;
    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.RegisterActivityForBugsense(Dummy.this);

        setContentView(R.layout.activity_splash);

        BGThread();


    }


    private void BGThread() {

        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                if (App.Utils.getString(Constants.user_id).equalsIgnoreCase("")) {
//                    Intent i = new Intent(Dummy.this, Login.class);
//                    startActivity(i);
                } else {
                    /*if (App.Utils.getString(Constants.is_email_verified).equals("1")) {
                        Intent i = new Intent(Dummy.this, Home.class);
                        startActivity(i);
                    } else {
                        Intent i = new Intent(Dummy.this, Login.class);
                        startActivity(i);
//                    }*/
//                    Intent i = new Intent(Dummy.this, Home.class);
//                    startActivity(i);
                }

                finish();
            }
        }, SPLASH_WAIT);
    }


}
