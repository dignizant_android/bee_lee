package com.beelee.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckBox;

import com.beelee.App;


public class RegularFontCheckBox extends CheckBox {
    public RegularFontCheckBox(Context context) {
        super(context);
        this.setTypeface(App.app_font_regular);
    }

    public RegularFontCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(App.app_font_regular);
    }

    public RegularFontCheckBox(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.setTypeface(App.app_font_regular);
    }
}
