package com.beelee.ui.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.CheckBox;

import com.beelee.App;


public class BoldFontCheckBox extends CheckBox {
    public BoldFontCheckBox(Context context) {
        super(context);
        this.setTypeface(App.app_font_medium, Typeface.BOLD);
    }

    public BoldFontCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(App.app_font_medium, Typeface.BOLD);
    }

    public BoldFontCheckBox(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.setTypeface(App.app_font_medium, Typeface.BOLD);
    }
}
