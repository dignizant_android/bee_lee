package com.beelee;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.beelee.constants.Constants;
import com.beelee.utility.CustomFont;
import com.beelee.utility.LogUtil;
import com.beelee.utility.MyUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.sql.Timestamp;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;


public class App extends Application {
    private static final String TAG = "App";
    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "3q3hHFayaeqfuad23WRSILWgN";
    private static final String TWITTER_SECRET = "nlp9jQXFpjIWF0hBxTPmsjhxDfNi2davzXe8LgBymHqFGIsFC8";


    public static Context context;
    public static MyUtils Utils;
    public static Locale locale;
    public static Typeface app_font_regular;
    public static Typeface app_font_light;
    public static Typeface app_font_bold;
    public static Typeface app_font_medium;

    //login user data
//    public static DBAdapter da;
    public InputStream databaseInputStream1;
    //asynhttp client

    public static OkHttpClient client;

    //GPS
//    public static GPSTracker gt;

    //GCM push notification
    String GCM_reg_id;

    public static final long REGISTRATION_EXPIRY_TIME_MS = 1000 * 3600 * 24 * 7;

    @SuppressWarnings("static-access")
    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        context = getApplicationContext();
        Utils = new MyUtils(context);

        /** GPS tracker for current location */
//        gt = new GPSTracker(this);
//        ReadDatabase();
        FirebaseApp.initializeApp(context);
//        getFCMRegID();
        InitializeHttpClient();
        InitializeFont();
        InitializeLanguage();


    }

    private static final String MULTIPART_FORM_DATA = "multipart/form-data";
    @NonNull
    public static RequestBody createRequestBody(@NonNull String s) {
        return RequestBody.create(
                MediaType.parse(MULTIPART_FORM_DATA), s);
    }

    @NonNull
    public static RequestBody createRequestBody(@NonNull File file) {
        return RequestBody.create(
                MediaType.parse(MULTIPART_FORM_DATA), file);
    }
    private void getFCMRegID() {
        String tkn = FirebaseInstanceId.getInstance().getToken();
//        Toast.makeText(getApplicationContext(), "Current Token [" + tkn + "]",
//                Toast.LENGTH_LONG).show();
        LogUtil.Print(TAG, "Current Token [" + tkn + "]");
    }
   /* private void ReadDatabase() {
        // TODO Auto-generated method stub

        try {

            da = new DBAdapter(context);
            File database = getApplicationContext().getDatabasePath(DBAdapter.DATABASE_NAME);

            if (!database.exists()) {
                // Database does not exist so copy it from assets here
                LogUtil.Print("Database", "Not Found");
                databaseInputStream1 = getAssets().open(DBAdapter.DATABASE_NAME);

                DBAdapter db = new DBAdapter(context);
                db.open();
                db.close();

                ImportDatabase ipd = new ImportDatabase(databaseInputStream1);
                ipd.copyDataBase();
                System.out.println("copying database .... ");

            } else {

                LogUtil.Print("Database", "Found");
            }

        } catch (IOException e) {

            e.printStackTrace();
        }

    }*/

    private void InitializeFont() {
        CustomFont font = new CustomFont(context);
        app_font_regular = font.font_regular;
        app_font_bold = font.font_bold;
        app_font_light = font.font_light;
        app_font_medium = font.font_medium;
    }

    public SSLContext getSslContext() {

        TrustManager[] byPassTrustManagers = new TrustManager[]{new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }

            public void checkClientTrusted(X509Certificate[] chain, String authType) {
            }

            public void checkServerTrusted(X509Certificate[] chain, String authType) {
            }
        }};

        SSLContext sslContext = null;

        try {
            sslContext = SSLContext.getInstance("TLS");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            sslContext.init(null, byPassTrustManagers, new SecureRandom());
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }

        return sslContext;

    }


    private void InitializeHttpClient() {

        client = new OkHttpClient.Builder()
                .addInterceptor(new BasicAuthInterceptor(Constants.AUTH_USERNAME, Constants.AUTH_PASSWORD))
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();
    }

    public static class BasicAuthInterceptor implements Interceptor {
        private String credentials;

        public BasicAuthInterceptor(String user, String password) {
            this.credentials = Credentials.basic(user, password);
        }

        @Override
        public okhttp3.Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            Request authenticatedRequest = request.newBuilder()
                    .header("Authorization", credentials).build();
            return chain.proceed(authenticatedRequest);
        }
    }



    public static void RegisterActivityForBugsense(Context context) {

        try {
//            Mint.initAndStartSession(context, Constants.Mint_key);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * set app language
     */
    private void InitializeLanguage() {
        if (App.Utils.getString(Constants.language).equals("")) {
            if (Locale.getDefault().getLanguage().equalsIgnoreCase(Constants.language_en)) {
                changeLang(Constants.language_en);
                App.Utils.putString(Constants.language, Constants.language_en);
            } else if (Locale.getDefault().getLanguage().equalsIgnoreCase(Constants.language_ar)) {
                changeLang(Constants.language_ar);
                App.Utils.putString(Constants.language, Constants.language_ar);
            } else {
                changeLang(Constants.language_en);
                App.Utils.putString(Constants.language, Constants.language_en);
            }
        } else {
            if (App.Utils.getString(Constants.language).equalsIgnoreCase(Constants.language_en)) {
                changeLang(Constants.language_en);
                App.Utils.putString(Constants.language, Constants.language_en);
            } else if (App.Utils.getString(Constants.language).equalsIgnoreCase(Constants.language_ar)) {
                changeLang(Constants.language_ar);
                App.Utils.putString(Constants.language, Constants.language_ar);
            } else {
                changeLang(Constants.language_en);
                App.Utils.putString(Constants.language, Constants.language_en);
            }

        }
        /*if (App.Utils.getString(Constants.language).equals("")) {
            if (Locale.getDefault().getLanguage().equalsIgnoreCase(Constants.language_en)) {
                changeLang(Constants.language_en);
                App.Utils.putString(Constants.language, Constants.language_en);
            } else if (Locale.getDefault().getLanguage().equalsIgnoreCase(Constants.language_ar)) {
                changeLang(Constants.language_ar);
                App.Utils.putString(Constants.language, Constants.language_ar);
            } else {
                changeLang(Constants.language_en);
                App.Utils.putString(Constants.language, Constants.language_en);
            }
        }
        else {
            if (App.USER_DETAILS.getLang().equalsIgnoreCase(Constants.language_en)) {
                changeLang(Constants.language_en);
                App.Utils.putString(Constants.language, Constants.language_en);
            } else if (App.USER_DETAILS.getLang().equalsIgnoreCase(Constants.language_ar)) {
                changeLang(Constants.language_ar);
                App.Utils.putString(Constants.language, Constants.language_ar);
            } else {
                changeLang(Constants.language_en);
                App.Utils.putString(Constants.language, Constants.language_en);
            }
        }*/
    }


    @SuppressWarnings("static-access")
    public static void changeLang(String lang) {
        Configuration config = context.getResources()
                .getConfiguration();

        App.Utils.putString(Constants.language, lang);

        if (lang.equals("1"))
            locale = new Locale("ar");
        else
            locale = new Locale("en");

        Locale.setDefault(locale);
        Configuration conf = new Configuration(config);
        conf.locale = locale;
        context.getResources().updateConfiguration(conf,
                context.getResources().getDisplayMetrics());

    }


    public static void ToggleView(boolean is_visible, View view) {

        if (view != null) {
            if (is_visible) {

                Animation in = AnimationUtils.loadAnimation(context, android.R.anim.fade_in);
                in.setDuration(1500);
                view.startAnimation(in);
                view.setVisibility(View.VISIBLE);
            } else {
                view.setVisibility(View.GONE);
            }
        }

    }

  /*  public void getGCMRegID() {
        if (Utils.getString(Constants.gcm_registration_id).equalsIgnoreCase("")) {

            GCM_reg_id = getRegistrationId(context);
            LogUtil.Print("Preference", "GCM_regId is : " + GCM_reg_id);
            if (GCM_reg_id.length() == 0) {

                String loading = context.getResources().getString(
                        R.string.txt_loading);
                // mProgressDialog = ProgressDialog.show(context, "", loading,
                // true, false);
                new registerBackground().execute(null, null, null);

                LogUtil.Print("length == 0", "GCM_regId is : " + GCM_reg_id);

            } else {
                Utils.putString(Constants.gcm_registration_id, GCM_reg_id);

                LogUtil.Print("length != 0", "GCM_regId is : " + GCM_reg_id);
                if (Utils.getString(Constants.device_id).equalsIgnoreCase("")
                        || Utils.getString(Constants.device_id) == null) {
                    Utils.putString(Constants.device_id, App.Utils.GetDeviceID());
                }
            }

            gcm = GoogleCloudMessaging.getInstance(context);
        } else {

            LogUtil.Print(TAG,
                    "getGCMRegID : else :"
                            + Utils.getString(Constants.gcm_registration_id));


        }
    }*/


    // ----------------------GCM---------------------------------------------------//
    private String getRegistrationId(Context context) {

        String registrationId = Utils.getString(Constants.gcm_registration_id);
        if (registrationId.length() == 0) {
            LogUtil.Print("gcm register not found", ".........Registration not found.");
            return "";
        }
        // check if app was updated; if so, it must clear registration id to
        // avoid a race condition if GCM sends a message
        int registeredVersion = Integer
                .parseInt(Utils.getString("appVersion"));
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion || isRegistrationExpired()) {
            LogUtil.Print("...", "App version changed or registration expired.");
            return "";
        }
        return registrationId;
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private boolean isRegistrationExpired() {

        long expirationTime = Long.parseLong(Utils.getString("onServerExpirationTimeMs"));
        return System.currentTimeMillis() > expirationTime;
    }


   /* private class registerBackground extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {

            String msg = "";
            try {

                if (gcm == null) {
                    gcm = GoogleCloudMessaging.getInstance(context);
                }

                GCM_reg_id = gcm.register(Constants.sender_id);
                LogUtil.Print("", "registerBackground : gcm reg id : " + GCM_reg_id);
                msg = "Device registered, registration id = " + GCM_reg_id;

                setRegistrationId(context, GCM_reg_id);

            } catch (IOException ex) {
                msg = "Error :" + ex.getMessage();
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String msg) {
            // if (mProgressDialog != null)
            // mProgressDialog.dismiss();

            if (!GCM_reg_id.equalsIgnoreCase("")) {

                LogUtil.Print("", "registerBackground : onPostExecute :gcm reg id : "
                        + GCM_reg_id);

                Utils.putString(Constants.gcm_registration_id, GCM_reg_id);
                if (Utils.getString(Constants.device_id).equalsIgnoreCase("")
                        || Utils.getString(Constants.device_id) == null) {
                    Utils.putString(Constants.device_id, App.Utils.GetDeviceID());
                }


            }

        }

    }*/

    private void setRegistrationId(Context context, String regId) {

        int appVersion = getAppVersion(context);

        LogUtil.Print("...", "Saving regId on app version " + appVersion);

        long expirationTime = System.currentTimeMillis()
                + REGISTRATION_EXPIRY_TIME_MS;
        Utils.putString(Constants.gcm_registration_id, regId);
        Utils.putString("appVersion", String.valueOf(appVersion));
        Utils.putString("onServerExpirationTimeMs",
                String.valueOf(expirationTime));
        LogUtil.Print("", "Setting registration expiry time to "
                + new Timestamp(expirationTime));

    }

    public static boolean IsValidEmail(String email2) {
        boolean check;
        Pattern p;
        Matcher m;

        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        p = Pattern.compile(EMAIL_STRING);

        m = p.matcher(email2);
        check = m.matches();

        return check;
    }

    public static boolean isArabicLanguage() {
        if (App.Utils.getString(Constants.language).equals(Constants.language_ar))
            return true;
        else
            return false;
    }



}
