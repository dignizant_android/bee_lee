package com.beelee.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.beelee.App;
import com.beelee.Interface.ApiInterface;
import com.beelee.Interface.OnEditTextChanged;
import com.beelee.R;
import com.beelee.adpater.FrgamentProductAdapter;
import com.beelee.constants.Api;
import com.beelee.constants.Constants;
import com.beelee.implementor.RecyclerViewItemClickListener;
import com.beelee.main.AllProductList;
import com.beelee.main.Home;
import com.beelee.model.DataProduct;
import com.beelee.model.GsonAddProductQuantity;
import com.beelee.model.GsonProductList;
import com.beelee.model.QuantityModel;
import com.beelee.utility.LogUtil;
import com.beelee.utility.MyUtils;
import com.beelee.utility.NavigatorManager;
import com.paginate.Paginate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ProductFragment extends Fragment implements RecyclerViewItemClickListener, View.OnClickListener {


    private static final String TAG = "ProductFragment";
    private List<DataProduct> list_Product = new ArrayList<>();
    public static List<DataProduct> list_Productall = new ArrayList<>();
    public static int total = 0;
    FrgamentProductAdapter fAdp;
    QuantityModel model;
    ProgressBar cpb;
    String offset = "0";
    Paginate paginate;
    private String title, sellerID;
    private int page;
    private TextView tv_err_msg;
    private RecyclerView rv_product;
    //    public static List<DataProduct> list = new ArrayList<>();
    private RecyclerView.LayoutManager mLayoutManager;
    private boolean isLoading = false;


    public ProductFragment() {
        // Required empty public constructor
    }

    // newInstance constructor for creating fragment with arguments
    public static ProductFragment newInstance(int page, String title, String seller_id) {
        ProductFragment fragmentFirst = new ProductFragment();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        args.putString("sellerID", seller_id);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        title = getArguments().getString("someTitle");
        sellerID = getArguments().getString("sellerID");
        page = getArguments().getInt("someInt");
        LogUtil.Print(TAG, "Title: " + title + " id: " + page + " Seller_id: " + sellerID);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_product_list, container, false);
        findViewByID(view);
//        if (list == null || list.size() == 0) {
//            AddMenu();
//        }
        main();
        offset = "0";
        setUpPagination();
        return view;
    }


    private void findViewByID(View view) {
        cpb = view.findViewById(R.id.cpb);
        tv_err_msg = view.findViewById(R.id.tv_err_msg);
        rv_product = view.findViewById(R.id.rv_product);
    }

    @Override
    public void onClick(View v) {

    }



    private void main() {

        mLayoutManager = new LinearLayoutManager(getActivity());
        rv_product.setLayoutManager(mLayoutManager);
        fAdp = new FrgamentProductAdapter(getActivity(), list_Product, new OnEditTextChanged() {
            @Override
            public void onTextChanged(int position, String charSeq) {
//                enteredNumber[position] = charSeq;
                LogUtil.Print("ProductAdapter", "onTextChanged:- " + charSeq);
                LogUtil.Print("ProductAdapter", "position:- " + position);
                list_Product.get(position).setNotes(charSeq.toString());

            }
        });
        fAdp.setOnRecyclerViewItemClickListener(this);
        rv_product.setAdapter(fAdp);

    }

    /*set Header*/
    private void setUpPagination() {

        if (paginate != null) {

            paginate.unbind();
        }

        paginate = Paginate.with(rv_product, new Paginate.Callbacks() {
            @Override
            public void onLoadMore() {
                isLoading = true;

                getProductListAPI();

            }


            @Override
            public boolean isLoading() {
                return isLoading;
            }

            @Override
            public boolean hasLoadedAllItems() {

                if (offset.equals(Constants.pagination_last_offset))
                    return true;
                else
                    return false;
            }
        }).setLoadingTriggerThreshold(2)
                .addLoadingListItem(true)
                .build();
    }

    private void getProductListAPI() {
        if (App.Utils.IsInternetOn()) {
            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.user_id, App.Utils.getString(Constants.user_id));
            params.put(Constants.access_token, App.Utils.getString(Constants.access_token));
            params.put(Constants.lang, Constants.language_en);
            params.put(Constants.seller_id, sellerID);
            params.put(Constants.category_id, title);
//            params.put(Constants.seller_id, "61749385");
            params.put(Constants.offset, offset);
            LogUtil.Print("My Product List ApiRequest params", "" + params);
            Call<GsonProductList> call = request.getProductList(params);

            if (list_Product.size() == 0) {
//                cpb.setVisibility(View.VISIBLE);
//                rl.setRefreshing(false);
            } else {
//                rl.setRefreshing(true);
            }

            call.enqueue(new Callback<GsonProductList>() {
                @Override
                public void onResponse(Call<GsonProductList> call, Response<GsonProductList> response) {
                    GsonProductList gsonProductList = response.body();
                    cpb.setVisibility(View.GONE);
                    if (gsonProductList.getFlag() == Constants.RESPONSE_SUCCESS_FLAG) {
                        tv_err_msg.setVisibility(View.GONE);

                        if (list_Product.size() == 0)
                            cpb.setVisibility(View.GONE);
//                        if (list_Product.size() > 0)
//                            list_Product.clear();
//                        rl.setRefreshing(false);

                        offset = "" + gsonProductList.getNextOffset();
                        list_Product.addAll(gsonProductList.getData());
                        list_Productall.addAll(gsonProductList.getData());
                        fAdp.notifyDataSetChanged();
                        isLoading = false;

                        if (paginate != null) {
                            if (offset.equals(Constants.pagination_last_offset))
                                paginate.setHasMoreDataToLoad(false);
                            else paginate.setHasMoreDataToLoad(true);
                        }

                    } else {
                        cpb.setVisibility(View.GONE);
                        tv_err_msg.setVisibility(View.VISIBLE);
                        tv_err_msg.setText("" + gsonProductList.getMsg());
//                        tv_err_msg.setText("No Vendor Found");
                        if (list_Product.size() > 0) {
                            list_Product.clear();
                            fAdp.notifyDataSetChanged();
                        }
                        isLoading = false;
                        if (paginate != null)
                            paginate.setHasMoreDataToLoad(false);
                    }
                }

                @Override
                public void onFailure(Call<GsonProductList> call, Throwable t) {
                    if (list_Product.size() == 0)
                        cpb.setVisibility(View.GONE);
                    LogUtil.Print("===Product List Error====", t.getMessage());
                }
            });

        } else {
            App.Utils.makeSnackbar(rv_product, getResources().getString(R.string.text_internet));

        }
    }

//    private void AddMenu() {
//        String[] menu = getResources().getStringArray(R.array.array_home_menu);
//        for (int i = 0; i < menu.length; i++) {
//            DataProduct modelResidentMenu = new DataProduct();
//            modelResidentMenu.setProductId(String.valueOf(i));
//            modelResidentMenu.setPrice("50");
//            modelResidentMenu.setQuantity(0);
//            list.add(modelResidentMenu);
//        }
//        LogUtil.Print(TAG, "size==============" + list.size());
//    }

    @Override
    public void onItemClick(int position, int flag, View view) {
        QuantityModel model = new QuantityModel();
        if (flag == Constants.ITEM_PLUS) {

            int j = list_Product.get(position).getQuantity();
            j++;
            list_Product.get(position).setQuantity(j);
            fAdp.notifyItemChanged(position);
            AllProductList.ShowTotal(getActivity(),position, j);

        } else if (flag == Constants.ITEM_MINUS) {

            int j = list_Product.get(position).getQuantity();
            j--;

            if (j < 0)
                MyUtils.makeToast("Quantity not acceptable");
            else {
                list_Product.get(position).setQuantity(j);
                fAdp.notifyItemChanged(position);
                AllProductList.ShowTotal(getActivity(),position, j);
            }

        }
    }



}
