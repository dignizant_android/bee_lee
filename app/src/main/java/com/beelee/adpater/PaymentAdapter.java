package com.beelee.adpater;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.beelee.R;
import com.beelee.constants.Constants;
import com.beelee.implementor.RecyclerViewItemClickListener;
import com.beelee.model.PaymentData;

import java.util.List;

/**
 * Created by Umesh on 1/9/2018.
 */
public class PaymentAdapter  extends RecyclerView.Adapter<PaymentAdapter.ViewHolder> {
    private static final String TAG = "PaymentAdapter";
    private List<PaymentData> list;
    private FragmentActivity activity;
    private RecyclerViewItemClickListener listener = null;


    public PaymentAdapter(FragmentActivity activity, List<PaymentData> list) {
        this.list = list;
        this.activity = activity;
    }

    @Override
    public PaymentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ll_payment, parent, false);
        PaymentAdapter.ViewHolder viewHolder = new PaymentAdapter.ViewHolder(view);


        return viewHolder;
    }
    public void updateList(List<PaymentData> listevent) {
        list = listevent;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(PaymentAdapter.ViewHolder holder, int position) {
        final PaymentData result = list.get(position);
        holder.tv_card_title.setText(result.getCategory());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tv_card_title;


        public ViewHolder(View itemView) {
            super(itemView);

            tv_card_title = itemView.findViewById(R.id.tv_card_title);

            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            if (v == itemView) {
                if (listener != null) {
                    listener.onItemClick(getAdapterPosition(), Constants.ITEM_CLICK, v);
                }
            }
        }


    }

    public void setOnRecyclerViewItemClickListener(RecyclerViewItemClickListener recyclerViewItemClickListener) {
        this.listener = recyclerViewItemClickListener;
    }


}
