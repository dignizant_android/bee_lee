package com.beelee.adpater;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.beelee.Interface.OnEditTextChanged;
import com.beelee.R;
import com.beelee.constants.Constants;
import com.beelee.implementor.RecyclerViewItemClickListener;
import com.beelee.model.DataDetailProduct;
import com.beelee.model.DataProduct;
import com.beelee.utility.ImageLoaderUtils;
import com.beelee.utility.RoundedCornersTransformation;
import com.beelee.utility.picasso.RoundCornerImageTransform;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by haresh on 2/12/18.
 */

public class ProductDetailAdapter extends RecyclerView.Adapter<ProductDetailAdapter.ViewHolder> {
    private static final String TAG = "ProductDetailAdapter";
    private List<DataDetailProduct> list;
    private FragmentActivity activity;
    private RecyclerViewItemClickListener listener = null;

    public ProductDetailAdapter(FragmentActivity activity, List<DataDetailProduct> list) {
        this.list = list;
        this.activity = activity;
    }

    @Override
    public ProductDetailAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ll_product_detail, parent, false);
        ProductDetailAdapter.ViewHolder viewHolder = new ProductDetailAdapter.ViewHolder(view);



        return viewHolder;
    }


    @Override
    public void onBindViewHolder(ProductDetailAdapter.ViewHolder holder, final int position) {
        final DataDetailProduct result = list.get(position);

        holder.tv_product_name.setText(result.getProductName());
//        Picasso.with(activity).load(result.getProductImage()).transform(new RoundCornerImageTransform(30, 0))
//                .error(R.drawable.placeholder_result_screen)
//                .placeholder(R.drawable.placeholder_result_screen)
//                .into(holder.iv_product);

        ImageLoaderUtils.loadImageFromURL(activity, String.valueOf(result.getProductImage()),
                holder.iv_product, R.drawable.placeholder_result_screen,
                new RoundedCornersTransformation(15, 2),
                R.dimen.side_menu_img_width, R.dimen.side_menu_img_width);

        holder.tv_price.setText(activity.getResources().getString(R.string.ils) + " " + result.getPrice());
        holder.tv_description.setText(result.getDescription());
        holder.tv_qty.setText(result.getQuantity());
        holder.tv_note.setText(result.getNotes());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setOnRecyclerViewItemClickListener(RecyclerViewItemClickListener recyclerViewItemClickListener) {
        this.listener = recyclerViewItemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {



        TextView tv_product_name, tv_price, tv_description,tv_qty,tv_note;
        ImageView iv_product;

        public ViewHolder(View itemView) {
            super(itemView);

            iv_product = itemView.findViewById(R.id.iv_product);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_product_name = itemView.findViewById(R.id.tv_product_name);
            tv_description = itemView.findViewById(R.id.tv_description);
            tv_qty = itemView.findViewById(R.id.tv_qty);
            tv_note = itemView.findViewById(R.id.tv_note);

            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            if (v == itemView) {
                if (listener != null) {
                    listener.onItemClick(getAdapterPosition(), Constants.ITEM_CLICK, v);
                }
            }
        }


    }


}

