package com.beelee.adpater;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.beelee.Interface.OnEditTextChanged;
import com.beelee.R;
import com.beelee.constants.Constants;
import com.beelee.implementor.RecyclerViewItemClickListener;
import com.beelee.model.DataProduct;
import com.beelee.utility.ImageLoaderUtils;
import com.beelee.utility.RoundedCornersTransformation;

import java.util.List;

/**
 * Created by haresh on 3/16/18.
 */

public class FrgamentProductAdapter extends RecyclerView.Adapter<FrgamentProductAdapter.ViewHolder> {
    private static final String TAG = "FrgamentProductAdapter";
    private List<DataProduct> list;
    private FragmentActivity activity;
    private RecyclerViewItemClickListener listener = null;
    private OnEditTextChanged onEditTextChanged;


    public FrgamentProductAdapter(FragmentActivity activity, List<DataProduct> list, OnEditTextChanged onEditTextChanged) {
        this.list = list;
        this.activity = activity;
        this.onEditTextChanged = onEditTextChanged;
    }

    @Override
    public FrgamentProductAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ll_products, parent, false);
        FrgamentProductAdapter.ViewHolder viewHolder = new FrgamentProductAdapter.ViewHolder(view);


        return viewHolder;
    }

//    public void updateList(List<PaymentAdapter> listGallery) {
//        list = listGallery;
//        notifyDataSetChanged();
//    }

    @Override
    public void onBindViewHolder(final FrgamentProductAdapter.ViewHolder holder, int position) {
        final DataProduct result = list.get(position);
        holder.tv_product_name.setText(result.getProductName());
//        Picasso.with(activity).load(result.getProductImage()).transform(new RoundCornerImageTransform(30, 0))
//                .error(R.drawable.placeholder_result_screen)
//                .placeholder(R.drawable.placeholder_result_screen)
//                .into(holder.iv_product);
        ImageLoaderUtils.loadImageFromURL(activity, String.valueOf(result.getProductImage()),
                holder.iv_product, R.drawable.placeholder_result_screen,
                new RoundedCornersTransformation(15, 2),
                R.dimen.side_menu_img_width, R.dimen.side_menu_img_width);

        holder.tv_price.setText(activity.getResources().getString(R.string.ils) + " " + result.getPrice());
        holder.tv_description.setText(result.getDescription());


        holder.et_qty.setText(String.valueOf(result.getQuantity()));
        holder.et_notes.setText(String.valueOf(result.getNotes()));
        holder.et_notes.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                onEditTextChanged.onTextChanged(holder.getAdapterPosition(), charSequence.toString());
//                result.setNotes(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setOnRecyclerViewItemClickListener(RecyclerViewItemClickListener recyclerViewItemClickListener) {
        this.listener = recyclerViewItemClickListener;
    }
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tv_product_name, tv_price, tv_description;
        ImageView iv_product, iv_minus, iv_plus;
        EditText et_notes, et_qty;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_product_name = itemView.findViewById(R.id.tv_product_name);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_description = itemView.findViewById(R.id.tv_description);
            iv_product = itemView.findViewById(R.id.iv_product);
            iv_minus = itemView.findViewById(R.id.iv_minus);
            iv_plus = itemView.findViewById(R.id.iv_plus);
            et_notes = itemView.findViewById(R.id.et_notes);
            et_qty = itemView.findViewById(R.id.et_qty);
            et_qty.setEnabled(false);
            et_notes.setEnabled(true);


            iv_minus.setOnClickListener(this);
            iv_plus.setOnClickListener(this);

            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            if (v == itemView) {
                if (listener != null) {
                    listener.onItemClick(getAdapterPosition(), Constants.ITEM_CLICK, v);
                }
            } else if (v == iv_minus) {
                listener.onItemClick(getAdapterPosition(), Constants.ITEM_MINUS, v);


            } else if (v == iv_plus) {
                listener.onItemClick(getAdapterPosition(), Constants.ITEM_PLUS, v);

            } else if (v == et_notes) {
                listener.onItemClick(getAdapterPosition(), Constants.ITEM_NOTES, v);

            }
        }


    }

}
