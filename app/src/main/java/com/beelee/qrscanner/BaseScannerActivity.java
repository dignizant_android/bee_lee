package com.beelee.qrscanner;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.beelee.App;
import com.beelee.Interface.ApiInterface;
import com.beelee.Login;
import com.beelee.Manifest;
import com.beelee.R;
import com.beelee.constants.Api;
import com.beelee.constants.Constants;
import com.beelee.main.ResultProduct;
import com.beelee.model.GsonMessage;
import com.beelee.utility.LogUtil;
import com.beelee.utility.MyUtils;
import com.beelee.utility.NavigatorManager;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BaseScannerActivity extends AppCompatActivity {
    public void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if(ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_logout:
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        BaseScannerActivity.this);
                builder.setTitle(getString(R.string.app_name));
                builder.setMessage(getResources().getString(R.string.dialog_logout));
                builder.setPositiveButton(
                        getResources().getString(R.string.text_yes),
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                CallLogoutAPI();
                            }
                        });
                builder.setNegativeButton(
                        getResources().getString(R.string.text_no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.setCanceledOnTouchOutside(false);
                alert.setCancelable(false);
                alert.show();

                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private void CallLogoutAPI() {
        if (App.Utils.IsInternetOn())
        {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.lang, Constants.language_en);
            params.put(Constants.user_id, App.Utils.getString(Constants.user_id));
            params.put(Constants.device_token, App.Utils.GetDeviceID());

            LogUtil.Print("Logout_params", "" + params);


            Call<GsonMessage> call = request.logoutuser(params);
            call.enqueue(new Callback<GsonMessage>() {
                @Override
                public void onResponse(Call<GsonMessage> call, Response<GsonMessage> response) {
                    GsonMessage gson = response.body();

                    if (gson.getFlag().equals(Constants.RESPONSE_SUCCESS_FLAG)) {

                        MyUtils.makeToast(gson.getMsg());
                        String devicetoken;
                        devicetoken = App.Utils.getString(Constants.device_token);


                        MyUtils.ClearAllPreferences();

                        App.Utils.putString(Constants.device_token, devicetoken);

                        //App.Utils.makeSnackbar(et_otp, gson.getMsg());
                        Intent intent = new Intent(BaseScannerActivity.this, Login.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        NavigatorManager.startNewActivity(BaseScannerActivity.this, intent);
                        finish();


                    } else if (gson.getFlag().equals(Constants.RESPONSE_UNSUCCESS_FLAG)) {
                        App.Utils.makeToast(gson.getMsg());
                    }
                }

                @Override
                public void onFailure(Call<GsonMessage> call, Throwable t) {
                    Log.d("Error", t.getMessage());

                }
            });

        } else {
            App.Utils.makeToast(getResources().getString(R.string.text_internet));

        }
    }
}
