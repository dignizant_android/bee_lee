package com.beelee;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.beelee.Interface.ApiInterface;
import com.beelee.constants.Api;
import com.beelee.constants.Constants;
import com.beelee.main.Home;
import com.beelee.main.Scanner;
import com.beelee.model.GsonRegister;
import com.beelee.utility.LogUtil;
import com.beelee.utility.MyUtils;
import com.beelee.utility.NavigatorManager;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity implements View.OnClickListener, LocationListener {

    private static final String TAG = "Login";
    TextView tv_new_acc;
    ImageView iv_back;
    Button btn_login;
    EditText et_mobile_no;
    ProgressBar cpb;

    LocationManager locationManager;
    private static final long MIN_DISTANCE_FOR_UPDATE = 10;
    private static final long MIN_TIME_FOR_UPDATE = 1000 * 60 * 2;

    String countryname = "notget";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.RegisterActivityForBugsense(Login.this);
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            window.setStatusBarColor(Color.TRANSPARENT);
//            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        setContentView(R.layout.activity_login);



        getLocation();
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(),
                android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
            return;

        }
        findViewById();
        main();

    }

    void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 5, this);
            }else{

            }

        }
        catch(SecurityException e) {
            e.printStackTrace();
        }
    }


    private void findViewById() {
        tv_new_acc = findViewById(R.id.tv_new_acc);
        iv_back = findViewById(R.id.iv_back);
        btn_login = findViewById(R.id.btn_login);
        et_mobile_no = findViewById(R.id.et_mobile_no);
        cpb = findViewById(R.id.cpb);

    }

    private void main() {
        tv_new_acc.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        btn_login.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == iv_back) {
            onBackPressed();
            finish();
        }
        if (view == btn_login) {
            if (et_mobile_no.getText().toString().isEmpty()) {
                et_mobile_no.requestFocus();
                MyUtils.makeSnackbar(et_mobile_no, getResources().getString(R.string.text_err_mobile));
            } else {
                CallLoginAPI();
            }
        }
        if (view == tv_new_acc) {
            Intent i = new Intent(Login.this, Register.class);
            i.putExtra(Constants.country, countryname);
            NavigatorManager.startNewActivity(Login.this, i);
        }

    }

    private void CallLoginAPI() {

        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.lang, Constants.language_en);
            params.put(Constants.mobile_no, et_mobile_no.getText().toString());
            params.put(Constants.device_token, App.Utils.GetDeviceID());
            params.put(Constants.register_id, App.Utils.getString(Constants.device_token));
            params.put(Constants.device_type, Constants.DEVICE_TYPE_ANDROID);
            LogUtil.Print("Login_params", "" + params);


            Call<GsonRegister> call = request.getuserLogin(params);
            cpb.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<GsonRegister>() {
                @Override
                public void onResponse(Call<GsonRegister> call, Response<GsonRegister> response) {
                    cpb.setVisibility(View.GONE);
                    GsonRegister gson = response.body();

                    if (gson.getFlag().equals(Constants.RESPONSE_SUCCESS_FLAG)) {

                        App.Utils.makeSnackbar(et_mobile_no, gson.getMsg());
                        App.Utils.putString(Constants.user_id, gson.getData().getUser_id());
                        App.Utils.putString(Constants.access_token, gson.getData().getAccess_token());

                        NavigatorManager.startNewActivity(Login.this, new Intent(Login.this, Home.class));
                        finish();

                    } else if (gson.getFlag().equals(Constants.RESPONSE_UNSUCCESS_FLAG)) {


                        if (gson.getMsg().equals("Invalid Username Or Password.")) {
                            App.Utils.makeSnackbar(et_mobile_no, gson.getMsg());
                        } else if (!gson.getUser_id().equals("")) {
                            Intent i = new Intent(Login.this, Verify_otp.class);
                            i.putExtra(Constants.mobile_no, et_mobile_no.getText().toString());
                            i.putExtra(Constants.user_id, gson.getUser_id());
                            NavigatorManager.startNewActivity(Login.this, i);
                            finish();
                        }
//
                    }
                }

                @Override
                public void onFailure(Call<GsonRegister> call, Throwable t) {
                    Log.d("Error", t.getMessage());

                }
            });

        } else {
            App.Utils.makeSnackbar(et_mobile_no, getResources().getString(R.string.text_internet));

        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    @Override
    public void onLocationChanged(Location location) {
        LogUtil.Print(TAG, "Latitude: " + location.getLatitude() + "\n Longitude: " + location.getLongitude());

        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            //MyUtils.makeToast(addresses.get(0).getCountryName());
            LogUtil.Print(TAG,"Name: "+addresses.get(0).getCountryName());
            countryname = addresses.get(0).getCountryName();

        }catch(Exception e)
        {

        }

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
