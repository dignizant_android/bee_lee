package com.beelee.firebase;

/**
 * Created by AndroidBash on 20-Aug-16.
 */

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.beelee.App;
import com.beelee.R;
import com.beelee.constants.Constants;
import com.beelee.main.DetailsProduct;
import com.beelee.main.ResultProduct;
import com.beelee.utility.LogUtil;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "FirebaseMessageService";
    public static int notifyID = 9001;
    private static int SPLASH_WAIT = 850;
    Bitmap bitmap;
    JSONObject customdata = new JSONObject();
    Handler handler = new Handler();
    NotificationManager notificationManager;
    private PendingIntent resultPendingIntent;
    private JSONObject jsonObject;
    private String product_id, price, comment, seller_id, is_flag,category_id;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        //

        Log.d(TAG, "From: " + remoteMessage.getFrom());
//        Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
        //     Log.d(TAG, "Notification Message Title: " + remoteMessage.getNotification().getTitle());


        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
//
////            {custom_data={"push_type":"0","send_from":"1","msg":"test by abc"}, message=Message from admin}
//            ActivityManager am = (ActivityManager) MyFirebaseMessagingService.this
//                    .getSystemService(Activity.ACTIVITY_SERVICE);
//            String packageName = am.getRunningTasks(1).get(0).topActivity
//                    .getPackageName();
//            if (packageName.contains(Constants.APP_PdfKG)) {
//                sendNotification(remoteMessage);
//            }
//
//            if (remoteMessage != null && !(App.Utils.getString(Constants.user_id)).equalsIgnoreCase("")) {
//
//                if (packageName.contains(Constants.APP_PdfKG)) {
//
//                    String message = remoteMessage.getData().get(Constants.message);
//
//                    Log.e("packageName!!!", packageName);
//
//                    NotificationCompat.Builder mNotifyBuilder;
//                    final NotificationManager mNotificationManager;
//                    int defaults = 0;
//                    defaults = defaults | Notification.DEFAULT_LIGHTS;
//                    defaults = defaults | Notification.DEFAULT_VIBRATE;
//                    defaults = defaults | Notification.DEFAULT_SOUND;
//
//                    if (Build.VERSION.SDK_INT >= 21) {
//
//                        mNotifyBuilder = new NotificationCompat.Builder(this)
//                                .setContentTitle(getResources().getString(R.string.app_name))
//                                .setContentText(message)
//                                .setSmallIcon(R.mipmap.ic_launcher)
//                                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
//                                .setColor(getApplicationContext().getResources().getColor(R.color.color_font));
//
//                    } else {
//
//                        mNotifyBuilder = new NotificationCompat.Builder(this)
//                                .setContentTitle(getResources().getString(R.string.app_name))
//                                .setContentText(message)
//                                .setSmallIcon(R.mipmap.ic_launcher);
//                    }
//
//                    mNotifyBuilder.setDefaults(defaults);
//                    mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//                    mNotificationManager.notify(1, mNotifyBuilder.build());
//
//                    handler.postDelayed(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                mNotificationManager.cancel(1);
//                                            }
//                                        }
//                            , SPLASH_WAIT);
//
//                    Intent broadcastIntent = new Intent();
//                    broadcastIntent.setAction(Constants.filterAction);
//                    try {
//                        jsonObject = new JSONObject(remoteMessage.getData().get("custom_data"));
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//
//                    broadcastIntent.putExtra(Constants.push_message, jsonObject.toString());
//                    broadcastIntent.putExtra(Constants.message, remoteMessage.getData().get(Constants.message));
//                    sendBroadcast(broadcastIntent);
//                   // LogUtil.Print(TAG,"Jason Data: "+jsonObject.toString());
//
//                } else {
//                    sendNotification(remoteMessage);
//                }
            sendNotification(remoteMessage);
        } else {

            LogUtil.Print("remoteMessage", "Null");
        }
    }
//        // Check if message contains a notification payload.
//        if (remoteMessage.getNotification() != null) {
//            Log.d(TAG, "Message Notification Title: " + remoteMessage.getNotification().getTitle());
//        }
//
//    }


    private void sendNotification(RemoteMessage bundle) {
        try {

            customdata = new JSONObject((bundle.getData().containsKey(Constants.custom_data)) ? bundle.getData().get(Constants.custom_data) : "");


            try {
                jsonObject = new JSONObject(bundle.getData().get("custom_data"));
                product_id = jsonObject.getString("product_id");
                price = jsonObject.getString("price");
                comment = jsonObject.getString("message");
                is_flag = jsonObject.getString("is_flag");

                LogUtil.Print(TAG, "JsonOBJEct: " + jsonObject);
                LogUtil.Print(TAG, "comment " + comment);
            } catch (JSONException e) {
                e.printStackTrace();
            }


            if (is_flag.equals("1")) {
                seller_id = jsonObject.getString(Constants.seller_id);
                category_id = jsonObject.getString(Constants.category_id);
                Intent resultIntent = new Intent(this, DetailsProduct.class);
                resultIntent.putExtra(Constants.product_id, product_id);
                resultIntent.putExtra(Constants.seller_id, seller_id);
                resultIntent.putExtra(Constants.category_id, category_id);
                resultIntent.putExtra(Constants.is_flag, is_flag);
                resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                resultPendingIntent = PendingIntent.getActivity(this, 0,
                        resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            } else if (is_flag.equals("0")) {
                Intent resultIntent = new Intent(this, ResultProduct.class);
                resultIntent.putExtra(Constants.product_id, product_id);
                resultIntent.putExtra(Constants.price, price);
                resultIntent.putExtra(Constants.comment, comment);
                resultIntent.putExtra(Constants.is_flag, is_flag);
                resultIntent.putExtra(Constants.message, bundle.getData().get(Constants.message));
                resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                resultPendingIntent = PendingIntent.getActivity(this, 0,
                        resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            }

            // resultIntent.putExtra(Constants.push_message, jsonObject.toString());


        } catch (JSONException e) {
            e.printStackTrace();
        }

        NotificationCompat.Builder mNotifyBuilder = null;
        NotificationManager mNotificationManager;

        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        String message = bundle.getData().get(Constants.message);


        if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            CharSequence adminChannelName = getString(R.string.app_name);
            String adminChannelDescription = comment;
            NotificationChannel adminChannel;
            adminChannel = new NotificationChannel(product_id, adminChannelName, NotificationManager.IMPORTANCE_LOW);
            adminChannel.setDescription(adminChannelDescription);
            adminChannel.enableLights(true);
            adminChannel.setLightColor(Color.RED);
            adminChannel.enableVibration(true);
            if (mNotificationManager != null) {
                mNotificationManager.createNotificationChannel(adminChannel);

//                PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 123, resultPendingIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                mNotifyBuilder = new NotificationCompat.Builder(this,product_id)
                        .setSmallIcon(R.mipmap.ic_launcher_transparent) //your app icon
                        .setBadgeIconType(R.mipmap.ic_launcher_transparent) //your app icon
                        .setChannelId(product_id)
                        .setContentTitle(getResources().getString(R.string.app_name))
                        .setAutoCancel(true).setContentIntent(resultPendingIntent)
                        .setNumber(1)
                        .setColor(255)
                        .setContentText(comment)
                        .setWhen(System.currentTimeMillis());
                mNotificationManager.notify(1, mNotifyBuilder.build());
            }


        }
        if (Build.VERSION.SDK_INT >= 21) {

            mNotifyBuilder = new NotificationCompat.Builder(this)
                    .setContentTitle(getResources().getString(R.string.app_name))
                    .setContentText(message)
                    //.setSmallIcon(R.mipmap.ic_launcher);
                    .setSmallIcon(R.mipmap.ic_launcher_transparent)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher_transparent))
                    .setColor(getApplicationContext().getResources().getColor(R.color.color_font));

        } else {

            mNotifyBuilder = new NotificationCompat.Builder(this)
                    .setContentTitle(getResources().getString(R.string.app_name))
                    .setContentText(message)
                    .setSmallIcon(R.mipmap.ic_launcher_transparent);

        }

        // Set pending intent
        mNotifyBuilder.setContentIntent(resultPendingIntent);

        // Set Vibrate, Sound and Light
        int defaults = 0;
        defaults = defaults | Notification.DEFAULT_LIGHTS;
        defaults = defaults | Notification.DEFAULT_VIBRATE;
        defaults = defaults | Notification.DEFAULT_SOUND;

        mNotifyBuilder.setDefaults(defaults);
        // Set the content for Notification

        if (bundle != null) {
            mNotifyBuilder.setContentText(message);
        }

        // Set autocancel
        mNotifyBuilder.setAutoCancel(true);
        // Post a notification

        notifyID = +1;
        App.Utils.putInt(Constants.notifyID, notifyID);

        // Set autocancel
        mNotifyBuilder.setAutoCancel(true);
        // Post a notification
        Random random = new Random();
        int randomNumber = random.nextInt(9999 - 1000) + 1000;
        Log.e("RandomNumber", "" + randomNumber);
        mNotificationManager.notify(randomNumber, mNotifyBuilder.build());

    }


}


