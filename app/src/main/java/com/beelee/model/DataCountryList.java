package com.beelee.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Umesh on 1/10/2018.
 */
public class DataCountryList {

    @SerializedName("country_id")
    @Expose
    private String country_id;
    @SerializedName("country_name")
    @Expose
    private String country_name;
    @SerializedName("country_code")
    @Expose
    private String country_code;
    @SerializedName("currencies_code")
    @Expose
    private String currencies_code;

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getCurrencies_code() {
        return currencies_code;
    }

    public void setCurrencies_code(String currencies_code) {
        this.currencies_code = currencies_code;
    }
}
