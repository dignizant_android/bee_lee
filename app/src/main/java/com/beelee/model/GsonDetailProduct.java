package com.beelee.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by haresh on 2/12/18.
 */

public class GsonDetailProduct {

    @SerializedName("flag")
    @Expose
    private Integer flag;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("next_offset")
    @Expose
    private Integer nextOffset;
    @SerializedName("total_price")
    @Expose
    private String totalPrice;
    @SerializedName("data")
    @Expose
    private List<DataDetailProduct> data = null;

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getNextOffset() {
        return nextOffset;
    }

    public void setNextOffset(Integer nextOffset) {
        this.nextOffset = nextOffset;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public List<DataDetailProduct> getData() {
        return data;
    }

    public void setData(List<DataDetailProduct> data) {
        this.data = data;
    }

}
