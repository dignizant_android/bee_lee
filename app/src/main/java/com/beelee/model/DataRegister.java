package com.beelee.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Umesh on 1/10/2018.
 */
public class DataRegister {
    @SerializedName("user_id")
    @Expose
    private String user_id;
    @SerializedName("country_id")
    @Expose
    private String country_id;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("mobile_no")
    @Expose
    private String mobile_no;
    @SerializedName("country_code")
    @Expose
    private String country_code;

    @SerializedName("birth_date")
    @Expose
    private String birth_date;

    @SerializedName("is_block")
    @Expose
    private String is_block;

    @SerializedName("is_flag")
    @Expose
    private String is_flag;

    @SerializedName("is_del")
    @Expose
    private String is_del;

    @SerializedName("date_added")
    @Expose
    private String date_added;

    @SerializedName("date_modified")
    @Expose
    private String date_modified;


    @SerializedName("access_token")
    @Expose
    private String access_token;

    @SerializedName("is_mobile_verified")
    @Expose
    private String is_mobile_verified;


    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public String getIs_block() {
        return is_block;
    }

    public void setIs_block(String is_block) {
        this.is_block = is_block;
    }

    public String getIs_flag() {
        return is_flag;
    }

    public void setIs_flag(String is_flag) {
        this.is_flag = is_flag;
    }

    public String getIs_del() {
        return is_del;
    }

    public void setIs_del(String is_del) {
        this.is_del = is_del;
    }

    public String getDate_added() {
        return date_added;
    }

    public void setDate_added(String date_added) {
        this.date_added = date_added;
    }

    public String getDate_modified() {
        return date_modified;
    }

    public void setDate_modified(String date_modified) {
        this.date_modified = date_modified;
    }

    public String getIs_mobile_verified() {
        return is_mobile_verified;
    }

    public void setIs_mobile_verified(String is_mobile_verified) {
        this.is_mobile_verified = is_mobile_verified;
    }


    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }
}
