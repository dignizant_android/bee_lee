package com.beelee.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by haresh on 2/9/18.
 */

public class DataProduct {

    @SerializedName("product_id")
    @Expose
    private String productId;

    @SerializedName("product_code")
    @Expose
    private String productCode;

    @SerializedName("product_name")
    @Expose
    private String productName;

    @SerializedName("category_id")
    @Expose
    private String category_id;

    @SerializedName("price")
    @Expose
    private String price;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("product_image")
    @Expose
    private String productImage;

    @SerializedName("qr_code_image")
    @Expose
    private String qrCodeImage;

    @SerializedName("seller_qr_code")
    @Expose
    private String sellerQrCode;

    @SerializedName("is_flag")
    @Expose
    private String isFlag;

    private int quantity;
    private String notes="";

    @SerializedName("all_categories")
    @Expose
    private String allCategories;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getQrCodeImage() {
        return qrCodeImage;
    }

    public void setQrCodeImage(String qrCodeImage) {
        this.qrCodeImage = qrCodeImage;
    }

    public String getSellerQrCode() {
        return sellerQrCode;
    }

    public void setSellerQrCode(String sellerQrCode) {
        this.sellerQrCode = sellerQrCode;
    }

    public String getIsFlag() {
        return isFlag;
    }

    public void setIsFlag(String isFlag) {
        this.isFlag = isFlag;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getAllCategories() {
        return allCategories;
    }

    public void setAllCategories(String allCategories) {
        this.allCategories = allCategories;
    }
}
