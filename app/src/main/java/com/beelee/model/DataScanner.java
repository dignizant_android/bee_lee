package com.beelee.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Umesh on 1/11/2018.
 */
public class DataScanner {

    @SerializedName("product_id")
    @Expose
    private String product_id;
    @SerializedName("user_id")
    @Expose
    private String user_id;
    @SerializedName("product_code")
    @Expose
    private String product_code;
    @SerializedName("seller_id")
    @Expose
    private String seller_id;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("time")
    @Expose
    private String time;

    @SerializedName("price")
    @Expose
    private String price;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("is_flag")
    @Expose
    private String is_flag;

    @SerializedName("is_del")
    @Expose
    private String is_del;

    @SerializedName("date_added")
    @Expose
    private String date_added;


    @SerializedName("seller_code")
    @Expose
    private String seller_code;


    @SerializedName("seller_name")
    @Expose
    private String seller_name;

    @SerializedName("product_name")
    @Expose
    private String product_name;

    @SerializedName("product_image")
    @Expose
    private String product_image;

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }


    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public String getSeller_id() {
        return seller_id;
    }

    public void setSeller_id(String seller_id) {
        this.seller_id = seller_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIs_flag() {
        return is_flag;
    }

    public void setIs_flag(String is_flag) {
        this.is_flag = is_flag;
    }

    public String getIs_del() {
        return is_del;
    }

    public void setIs_del(String is_del) {
        this.is_del = is_del;
    }

    public String getDate_added() {
        return date_added;
    }

    public void setDate_added(String date_added) {
        this.date_added = date_added;
    }

    public String getSeller_code() {
        return seller_code;
    }

    public void setSeller_code(String seller_code) {
        this.seller_code = seller_code;
    }

    public String getSeller_name() {
        return seller_name;
    }

    public void setSeller_name(String seller_name) {
        this.seller_name = seller_name;
    }

}
