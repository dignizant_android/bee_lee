package com.beelee.model;

/**
 * Created by Umesh on 1/9/2018.
 */
public class PaymentData {
    int id;
    String category;

    public PaymentData() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
