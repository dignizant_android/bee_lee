package com.beelee.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Created by Umesh on 1/18/2018.
 */
public class GsonOtp {


    @SerializedName("otp_code")
    @Expose
    private String otpCode;
    @SerializedName("date_added")
    @Expose
    private String dateAdded;

    public String getOtpCode() {
        return otpCode;
    }

    public void setOtpCode(String otpCode) {
        this.otpCode = otpCode;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }
}
