package com.beelee.main;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.beelee.App;
import com.beelee.Interface.ApiInterface;
import com.beelee.R;
import com.beelee.adpater.ProductabAdapter;
import com.beelee.constants.Api;
import com.beelee.constants.Constants;
import com.beelee.fragment.ProductFragment;
import com.beelee.model.DataCategory;
import com.beelee.model.GsonAddProductQuantity;
import com.beelee.model.GsonCategory;
import com.beelee.model.QuantityModel;
import com.beelee.utility.LogUtil;
import com.beelee.utility.MyUtils;
import com.beelee.utility.NavigatorManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllProductList extends AppCompatActivity implements View.OnClickListener {

    public static TextView tv_total;
    private TextView tv_submit;
    public static List<DataCategory> list_Category = new ArrayList<>();
    public static ArrayList<HashMap<String, String>> product_array = new ArrayList<>();
    QuantityModel model;
    //header
    ImageView iv_header_navigation;
    TextView tv_title;
    TabLayout tab_product;
    ViewPager vp_product;
    int pager_position = 0;
    ProgressBar cpb;
    ProductabAdapter productabAdapter;
    Runnable mTabLayout_config = new Runnable() {
        @Override
        public void run() {

            int tabLayoutWidth = tab_product.getWidth();

            DisplayMetrics metrics = new DisplayMetrics();
            AllProductList.this.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            int deviceWidth = metrics.widthPixels;

            if (tabLayoutWidth < deviceWidth) {
                tab_product.setTabMode(TabLayout.MODE_FIXED);
                tab_product.setTabGravity(TabLayout.GRAVITY_FILL);
            } else {
                tab_product.setTabMode(TabLayout.MODE_SCROLLABLE);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_product_list);

        setupheaderview();
        findViewById();
        callCategeryListAPI();
        main();
    }

    private void setupheaderview() {
        iv_header_navigation = findViewById(R.id.iv_header_navigation);
        tv_title = findViewById(R.id.tv_title);
        tv_title.setText(getResources().getString(R.string.text_listing));
        iv_header_navigation.setOnClickListener(this);
    }

    private void findViewById() {
        cpb = findViewById(R.id.cpb);
        tab_product = findViewById(R.id.tab_product);
        vp_product = findViewById(R.id.vp_product);
        tv_total = findViewById(R.id.tv_total);
        tv_submit = findViewById(R.id.tv_submit);
        tv_submit.setOnClickListener(this);
    }

    private void main() {

//        tab_product.setTabMode(TabLayout.MODE_SCROLLABLE);
    }

    @Override
    public void onClick(View v) {
        if (v == iv_header_navigation) {
            onBackPressed();
            ProductFragment.list_Productall.clear();
            finish();
        } else if (v == tv_submit) {
            if (tv_total.getText().toString().equals("0") || tv_total.getText().toString().equals(getResources().getString(R.string.ils) + " " + "0")) {
                MyUtils.makeSnackbar(vp_product, "Please select Any One Product");
            } else {
                product_array.clear();
                for (int i = 0; i < ProductFragment.list_Productall.size(); i++) {
                    HashMap<String, String> map = new HashMap<>();
                    map.put(Constants.product_id, ProductFragment.list_Productall.get(i).getProductId());
                    map.put(Constants.category_id, ProductFragment.list_Productall.get(i).getCategory_id());
                    map.put(Constants.quantity, String.valueOf(ProductFragment.list_Productall.get(i).getQuantity()));
                    map.put(Constants.notes, ProductFragment.list_Productall.get(i).getNotes());
                    product_array.add(map);
                }

                LogUtil.Print("Product Array", "" + product_array);

                CallProductSentAPI();

            }

        }
    }

    private void callCategeryListAPI() {
        if (App.Utils.IsInternetOn()) {
            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.user_id, App.Utils.getString(Constants.user_id));
            params.put(Constants.access_token, App.Utils.getString(Constants.access_token));
            params.put(Constants.lang, Constants.language_en);
            params.put(Constants.seller_id, getIntent().getExtras().getString(Constants.seller_id));
            LogUtil.Print("My Category List ApiRequest params", "" + params);
            Call<GsonCategory> call = request.getCategoryList(params);

            if (list_Category.size() == 0) {
                cpb.setVisibility(View.VISIBLE);
//                rl.setRefreshing(false);
            } else {
//                rl.setRefreshing(true);
            }

            call.enqueue(new Callback<GsonCategory>() {
                @Override
                public void onResponse(Call<GsonCategory> call, Response<GsonCategory> response) {
                    GsonCategory gson = response.body();
                    cpb.setVisibility(View.GONE);
                    if (gson.getFlag() == Constants.RESPONSE_SUCCESS_FLAG) {

                        if (list_Category.size()>0)
                            list_Category.clear();
                        list_Category.addAll(gson.getData());
                        setupViewPager(vp_product);
                        tab_product.setupWithViewPager(vp_product);
                        tab_product.post(mTabLayout_config);
//                        pAdp.notifyDataSetChanged();


                    } else {
//                        cpb.setVisibility(View.GONE);
//                        tv_err_msg.setVisibility(View.VISIBLE);
//                        tv_err_msg.setText("" + gsonProductList.getMsg());
//                        tv_err_msg.setText("No Vendor Found");
                        if (list_Category.size() > 0) {
                            list_Category.clear();
//                            pAdp.notifyDataSetChanged();
                        }
                    }
                }

                @Override
                public void onFailure(Call<GsonCategory> call, Throwable t) {
                    if (list_Category.size() == 0)
                        cpb.setVisibility(View.GONE);
                        LogUtil.Print("===Product List Error====", t.getMessage());
                }
            });

        } else {
            App.Utils.makeSnackbar(vp_product, getResources().getString(R.string.text_internet));

        }
    }

    private void setupViewPager(ViewPager viewPager) {
        int j;
        productabAdapter = new ProductabAdapter(getSupportFragmentManager());
        for (j = 0; j < list_Category.size(); j++) {
            productabAdapter.addFragment(new ProductFragment().newInstance(j, list_Category.get(j).getCategoryId(), getIntent().getExtras().getString(Constants.seller_id)), list_Category.get(j).getCategoryName());
        }
        viewPager.setAdapter(productabAdapter);
        viewPager.setOffscreenPageLimit(j);
        viewPager.setCurrentItem(0, true);


    }

    private void CallProductSentAPI() {


        if (App.Utils.IsInternetOn()) {


            LogUtil.Print("Product Model: ", "" + product_array);
            model = new QuantityModel();
            model.setProductJSONArray(MyUtils.createJSONArrayFromProductList(product_array));


            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.user_id, App.Utils.getString(Constants.user_id));
            params.put(Constants.access_token, App.Utils.getString(Constants.access_token));
            params.put(Constants.lang, Constants.language_en);
            params.put(Constants.key, model.getProductJSONArray());

            LogUtil.Print("Model: ", "" + model.getProductJSONArray());
            LogUtil.Print("Product Quantity ApiRequest params", "" + params);

            Call<GsonAddProductQuantity> call = request.setProductQuantity(params);

            if (ProductFragment.list_Productall.size() == 0) {
                cpb.setVisibility(View.VISIBLE);
//                rl.setRefreshing(false);
            }
            call.enqueue(new Callback<GsonAddProductQuantity>() {
                @Override
                public void onResponse(Call<GsonAddProductQuantity> call, Response<GsonAddProductQuantity> response) {
                    GsonAddProductQuantity gsonProductList = response.body();
                    if (gsonProductList.getFlag() == Constants.RESPONSE_SUCCESS_FLAG) {


                        cpb.setVisibility(View.GONE);

                        AlertDialog.Builder builder = new AlertDialog.Builder(AllProductList.this);
                        builder.setTitle(R.string.app_name);
                        builder.setMessage(R.string.text_push)
                                .setCancelable(false)
                                .setPositiveButton(getResources().getString(R.string.text_ok), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //car_poolig_id = list.get(position).getCarpooling_details_id();
                                        dialog.cancel();
                                        Intent intent = new Intent(AllProductList.this, Home.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        NavigatorManager.startNewActivity(AllProductList.this, intent);
                                        finish();
                                    }
                                });

                        AlertDialog alert = builder.create();
                        alert.show();


                    } else {
                        cpb.setVisibility(View.GONE);
                        MyUtils.makeSnackbar(vp_product, gsonProductList.getMsg());

                    }
                }

                @Override
                public void onFailure(Call<GsonAddProductQuantity> call, Throwable t) {
                    cpb.setVisibility(View.GONE);
                    LogUtil.Print("===Product Sent Error====", t.getMessage());
                }
            });

        } else {
            App.Utils.makeSnackbar(vp_product, getResources().getString(R.string.text_internet));

        }

    }

    public static void ShowTotal(Context ctx, int position, int j) {
        int total = 0;
        for (int i = 0; i < ProductFragment.list_Productall.size(); i++) {
            int k = ProductFragment.list_Productall.get(i).getQuantity();
            int price = Integer.parseInt(ProductFragment.list_Productall.get(i).getPrice());
            total += price * k;
        }
        tv_total.setText(ctx.getResources().getString(R.string.ils) + " " + total);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ProductFragment.list_Productall.clear();
        finish();
    }
}
