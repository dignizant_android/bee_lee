package com.beelee.main;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.beelee.App;
import com.beelee.Interface.ApiInterface;
import com.beelee.R;
import com.beelee.constants.Api;
import com.beelee.constants.Constants;
import com.beelee.model.GsonScanner;
import com.beelee.utility.ImageLoaderUtils;
import com.beelee.utility.LogUtil;
import com.beelee.utility.NavigatorManager;
import com.beelee.utility.RoundedCornersTransformation;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResultProduct extends AppCompatActivity implements View.OnClickListener {

    TextView tv_title, tv_scan_code, tv_product_name, tv_seller_name, tv_price, tv_comment, tv_waiting;
    ImageView iv_header_navigation, iv_product_img, iv_waiting;
    Button btn_pay;
    LinearLayout lv_price;
    ProgressBar cpb;


    String product_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.RegisterActivityForBugsense(ResultProduct.this);

        setContentView(R.layout.activity_result_product);

        setupheaderview();
        findViewById();

        if (getIntent() != null) {
            product_id = getIntent().getExtras().getString(Constants.product_id);
            lv_price.setVisibility(View.VISIBLE);
            tv_price.setText(getResources().getString(R.string.ils) + " " + getIntent().getExtras().getString(Constants.price));
            tv_comment.setText(getIntent().getExtras().getString(Constants.comment));
        }
        main();
    }


    private void setupheaderview() {
        tv_title = findViewById(R.id.tv_title);
        iv_header_navigation = findViewById(R.id.iv_header_navigation);

        tv_title.setText(getResources().getString(R.string.text_result));
        iv_header_navigation.setOnClickListener(this);
    }

    private void findViewById() {
        cpb = findViewById(R.id.cpb);
        btn_pay = findViewById(R.id.btn_pay);
        tv_scan_code = findViewById(R.id.tv_scan_code);
        tv_product_name = findViewById(R.id.tv_product_name);
        tv_seller_name = findViewById(R.id.tv_seller_name);
        tv_price = findViewById(R.id.tv_price);
        tv_comment = findViewById(R.id.tv_comment);
        iv_product_img = findViewById(R.id.iv_product_img);
        lv_price = findViewById(R.id.lv_price);


        tv_waiting = findViewById(R.id.tv_waiting);
        iv_waiting = findViewById(R.id.iv_waiting);


    }

    private void main() {
        //tv_scan_code.setText(getIntent().getExtras().getString(Constants.qrcode));
        CallProductDetailAPI();

        btn_pay.setOnClickListener(this);

    }

    private void CallProductDetailAPI() {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.lang, Constants.language_en);
            params.put(Constants.user_id, App.Utils.getString(Constants.user_id));
            params.put(Constants.access_token, App.Utils.getString(Constants.access_token));
            params.put(Constants.product_id, product_id);
            LogUtil.Print("Result_Scanner_params", "" + params);


            Call<GsonScanner> call = request.productDetail(params);
            cpb.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<GsonScanner>() {
                @Override
                public void onResponse(Call<GsonScanner> call, Response<GsonScanner> response) {
                    cpb.setVisibility(View.GONE);
                    GsonScanner gson = response.body();

                    if (gson.getFlag().equals(Constants.RESPONSE_SUCCESS_FLAG)) {
                        tv_product_name.setText(gson.getData().getProduct_name());
                        tv_scan_code.setText(gson.getData().getProduct_code());
                        tv_seller_name.setText(gson.getData().getSeller_name());
                        tv_comment.setText(gson.getData().getDescription());
                        ImageLoaderUtils.loadImageFromURL(getApplicationContext(), String.valueOf(gson.getData().getProduct_image()),
                                iv_product_img, R.drawable.placeholder_result_screen,
                                new RoundedCornersTransformation(15, 2),
                                R.dimen.product_img_width, R.dimen.product_img_width);

                        LogUtil.Print("description", gson.getData().getDescription());


                    } else if (gson.getFlag().equals(Constants.RESPONSE_UNSUCCESS_FLAG)) {
                        App.Utils.makeToast(gson.getMsg());
                    }
                }

                @Override
                public void onFailure(Call<GsonScanner> call, Throwable t) {
                    Log.d("Error", t.getMessage());

                }
            });

        } else {
            App.Utils.makeToast(getResources().getString(R.string.text_internet));

        }
    }

    @Override
    public void onClick(View view) {
        if (view == iv_header_navigation) {
            onBackPressed();
            finish();
        }
        if (view == btn_pay) {
            Intent i = new Intent(ResultProduct.this, PaymentOption.class);
            i.putExtra(Constants.price, getIntent().getExtras().getString(Constants.price));
            i.putExtra(Constants.product_id, product_id);
            i.putExtra(Constants.is_flag, getIntent().getExtras().getString(Constants.is_flag));
            NavigatorManager.startNewActivity(ResultProduct.this, i);
        }

    }
}
