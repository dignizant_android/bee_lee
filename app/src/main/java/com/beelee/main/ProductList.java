package com.beelee.main;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.beelee.App;
import com.beelee.Interface.ApiInterface;
import com.beelee.Interface.OnEditTextChanged;
import com.beelee.Login;
import com.beelee.R;
import com.beelee.adpater.ProductAdapter;
import com.beelee.constants.Api;
import com.beelee.constants.Constants;
import com.beelee.implementor.RecyclerViewItemClickListener;
import com.beelee.model.DataProduct;
import com.beelee.model.GsonAddProductQuantity;
import com.beelee.model.GsonProductList;
import com.beelee.model.QuantityModel;
import com.beelee.qrscanner.BaseScannerActivity;
import com.beelee.utility.LogUtil;
import com.beelee.utility.MyUtils;
import com.beelee.utility.NavigatorManager;
import com.google.gson.Gson;
import com.paginate.Paginate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductList extends AppCompatActivity implements View.OnClickListener, RecyclerViewItemClickListener {

    public static List<DataProduct> list_Product = new ArrayList<>();
    //header
    ImageView iv_header_navigation;
    TextView tv_title, tv_err_msg, tv_total, tv_submit,tv_pay;
    // SwipeRefreshLayout rl;
    RecyclerView rv_product;
    ProgressBar cpb;
    String offset = "0";
    Paginate paginate;
    ProductAdapter pAdp;
    QuantityModel model;
    private boolean isLoading = false;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<HashMap<String, String>> product_array = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        setupheaderview();
        findViewById();
        main();
        offset = "0";
        list_Product.clear();
        setUpPagination();
    }

    private void setupheaderview() {
        iv_header_navigation = findViewById(R.id.iv_header_navigation);
        tv_title = findViewById(R.id.tv_title);
        tv_title.setText(getResources().getString(R.string.text_listing));
        iv_header_navigation.setOnClickListener(this);

    }

    private void findViewById() {
        cpb = findViewById(R.id.cpb);
//        rl = findViewById(R.id.rl);
        rv_product = findViewById(R.id.rv_product);
        tv_err_msg = findViewById(R.id.tv_err_msg);
        tv_total = findViewById(R.id.tv_total);
        tv_submit = findViewById(R.id.tv_submit);
        tv_pay = findViewById(R.id.tv_pay);
        tv_submit.setVisibility(View.VISIBLE);
        tv_pay.setVisibility(View.GONE);
        tv_submit.setOnClickListener(this);

//        rl.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW);
//        rl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                offset = "0";
//                list_Product.clear();
//                getProductListAPI();
//
//            }
//        });
    }

    private void main() {
//        final String[] enteredNumber = new String[0];
        mLayoutManager = new LinearLayoutManager(ProductList.this);
        rv_product.setLayoutManager(mLayoutManager);
        pAdp = new ProductAdapter(ProductList.this, list_Product, new OnEditTextChanged() {
            @Override
            public void onTextChanged(int position, String charSeq) {
//                enteredNumber[position] = charSeq;
                LogUtil.Print("ProductAdapter","onTextChanged:- "+charSeq);
                LogUtil.Print("ProductAdapter","position:- "+position);
                list_Product.get(position).setNotes(charSeq.toString());

            }
        });
        pAdp.setOnRecyclerViewItemClickListener(this);
        rv_product.setAdapter(pAdp);
    }

    @Override
    public void onClick(View view) {
        if (view == iv_header_navigation) {
            onBackPressed();
            finish();
        } else if (view == tv_submit) {
            if (tv_total.getText().toString().equals("0") || tv_total.getText().toString().equals(getResources().getString(R.string.ils) + " " + "0")) {
                MyUtils.makeSnackbar(rv_product, "Please select Any One Product");
            } else {
                product_array.clear();
                for (int i = 0; i < list_Product.size(); i++) {
                    HashMap<String, String> map = new HashMap<>();
                    map.put(Constants.product_id, list_Product.get(i).getProductId());
                    map.put(Constants.quantity, String.valueOf(list_Product.get(i).getQuantity()));
                    map.put(Constants.notes, list_Product.get(i).getNotes());
                    product_array.add(map);
                }

                LogUtil.Print("Product Array",""+product_array);

                CallProductSentAPI();

            }

        }
    }

    private void CallProductSentAPI() {


        if (App.Utils.IsInternetOn()) {


            LogUtil.Print("Product Model: ", "" + product_array);
            model = new QuantityModel();
            model.setProductJSONArray(MyUtils.createJSONArrayFromProductList(product_array));


            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.user_id, App.Utils.getString(Constants.user_id));
            params.put(Constants.access_token, App.Utils.getString(Constants.access_token));
            params.put(Constants.lang, Constants.language_en);
            params.put(Constants.key, model.getProductJSONArray());

            LogUtil.Print("Model: ", "" + model.getProductJSONArray());
            LogUtil.Print("Product Quantity ApiRequest params", "" + params);

            Call<GsonAddProductQuantity> call = request.setProductQuantity(params);

            if (list_Product.size() == 0) {
                cpb.setVisibility(View.VISIBLE);
//                rl.setRefreshing(false);
            }
            call.enqueue(new Callback<GsonAddProductQuantity>() {
                @Override
                public void onResponse(Call<GsonAddProductQuantity> call, Response<GsonAddProductQuantity> response) {
                    GsonAddProductQuantity gsonProductList = response.body();
                    if (gsonProductList.getFlag() == Constants.RESPONSE_SUCCESS_FLAG) {


                        cpb.setVisibility(View.GONE);

                        AlertDialog.Builder builder = new AlertDialog.Builder(ProductList.this);
                        builder.setTitle(R.string.app_name);
                        builder.setMessage(R.string.text_push)
                                .setCancelable(false)
                                .setPositiveButton(getResources().getString(R.string.text_ok), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //car_poolig_id = list.get(position).getCarpooling_details_id();
                                        dialog.cancel();
                                        Intent intent = new Intent(ProductList.this, Home.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        NavigatorManager.startNewActivity(ProductList.this, intent);
                                        finish();
                                    }
                                });

                        AlertDialog alert = builder.create();
                        alert.show();



                    } else {
                        cpb.setVisibility(View.GONE);
                        MyUtils.makeSnackbar(rv_product, gsonProductList.getMsg());

                    }
                }

                @Override
                public void onFailure(Call<GsonAddProductQuantity> call, Throwable t) {
                        cpb.setVisibility(View.GONE);
                    LogUtil.Print("===Product Sent Error====", t.getMessage());
                }
            });

        } else {
            App.Utils.makeSnackbar(rv_product, getResources().getString(R.string.text_internet));

        }

    }

    /*set Header*/
    private void setUpPagination() {

        if (paginate != null) {

            paginate.unbind();
        }

        paginate = Paginate.with(rv_product, new Paginate.Callbacks() {
            @Override
            public void onLoadMore() {
                isLoading = true;

                getProductListAPI();

            }


            @Override
            public boolean isLoading() {
                return isLoading;
            }

            @Override
            public boolean hasLoadedAllItems() {

                if (offset.equals(Constants.pagination_last_offset))
                    return true;
                else
                    return false;
            }
        }).setLoadingTriggerThreshold(2)
                .addLoadingListItem(true)
                .build();
    }

    private void getProductListAPI() {
        if (App.Utils.IsInternetOn()) {
            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.user_id, App.Utils.getString(Constants.user_id));
            params.put(Constants.access_token, App.Utils.getString(Constants.access_token));
            params.put(Constants.lang, Constants.language_en);
            params.put(Constants.seller_id, getIntent().getExtras().getString(Constants.seller_id));
//            params.put(Constants.seller_id, "61749385");
            params.put(Constants.offset, offset);
            LogUtil.Print("My Product List ApiRequest params", "" + params);
            Call<GsonProductList> call = request.getProductList(params);

            if (list_Product.size() == 0) {
                cpb.setVisibility(View.VISIBLE);
//                rl.setRefreshing(false);
            } else {
//                rl.setRefreshing(true);
            }

            call.enqueue(new Callback<GsonProductList>() {
                @Override
                public void onResponse(Call<GsonProductList> call, Response<GsonProductList> response) {
                    GsonProductList gsonProductList = response.body();
                    if (gsonProductList.getFlag() == Constants.RESPONSE_SUCCESS_FLAG) {
                        tv_err_msg.setVisibility(View.GONE);
                        if (list_Product.size() == 0)
                            cpb.setVisibility(View.GONE);
//                        if (list_Product.size() > 0)
//                            list_Product.clear();
//                        rl.setRefreshing(false);

                        offset = "" + gsonProductList.getNextOffset();
                        list_Product.addAll(gsonProductList.getData());
                        pAdp.notifyDataSetChanged();
                        isLoading = false;

                        if (paginate != null) {
                            if (offset.equals(Constants.pagination_last_offset))
                                paginate.setHasMoreDataToLoad(false);
                            else paginate.setHasMoreDataToLoad(true);
                        }

                    } else {
                        cpb.setVisibility(View.GONE);
                        tv_err_msg.setVisibility(View.VISIBLE);
                        tv_err_msg.setText("" + gsonProductList.getMsg());
//                        tv_err_msg.setText("No Vendor Found");
                        if (list_Product.size() > 0) {
                            list_Product.clear();
                            pAdp.notifyDataSetChanged();
                        }
                        isLoading = false;
                        if (paginate != null)
                            paginate.setHasMoreDataToLoad(false);
                    }
                }

                @Override
                public void onFailure(Call<GsonProductList> call, Throwable t) {
                    if (list_Product.size() == 0)
                        cpb.setVisibility(View.GONE);
                    LogUtil.Print("===Product List Error====", t.getMessage());
                }
            });

        } else {
            App.Utils.makeSnackbar(rv_product, getResources().getString(R.string.text_internet));

        }
    }


    @Override
    public void onItemClick(int position, int flag, View view) {

        QuantityModel model = new QuantityModel();
        if (flag == Constants.ITEM_PLUS) {

            int j = list_Product.get(position).getQuantity();
            j++;
            list_Product.get(position).setQuantity(j);
            pAdp.notifyItemChanged(position);
            ShowTotal(position, j);
//            LogUtil.Print("Plus:--",""+model.get()+"j ==="+j);
        } else if (flag == Constants.ITEM_MINUS) {

            int j = list_Product.get(position).getQuantity();
            j--;

            if (j < 0)
                MyUtils.makeToast("Quantity not acceptable");
            else {
                list_Product.get(position).setQuantity(j);
                pAdp.notifyItemChanged(position);
                ShowTotal(position, j);
            }

//            LogUtil.Print("Minus:--",""+model.getQty()+"j ==="+j);
        } else if (flag == Constants.ITEM_NOTES) {


        }

    }

    private void ShowTotal(int position, int j) {
        int total = 0;
        for (int i = 0; i < list_Product.size(); i++) {
            int k = list_Product.get(i).getQuantity();
            int price = Integer.parseInt(list_Product.get(i).getPrice());
            total += price * k;
        }
        tv_total.setText(getResources().getString(R.string.ils) + " " + total);

        //tv_total.setText(String.valueOf(Integer.parseInt(list.get(position).getCategory()) * j));
    }
}
