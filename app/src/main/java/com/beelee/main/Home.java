package com.beelee.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.beelee.R;
import com.beelee.constants.Constants;
import com.beelee.utility.NavigatorManager;

public class Home extends AppCompatActivity implements View.OnClickListener {

    Button btn_detail, btn_pay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        findViewById();
    }

    private void findViewById() {
        btn_pay = findViewById(R.id.btn_pay);
        btn_detail = findViewById(R.id.btn_detail);
        btn_detail.setOnClickListener(this);
        btn_pay.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == btn_detail) {
            Intent detailintent = new Intent(Home.this, Scanner.class);
            detailintent.putExtra(Constants.data, Constants.homeDetail);
            NavigatorManager.startNewActivity(Home.this, detailintent);
        } else if (view == btn_pay) {
            Intent payintent = new Intent(Home.this, Scanner.class);
            payintent.putExtra(Constants.data, Constants.homePay);
            NavigatorManager.startNewActivity(Home.this, payintent);
        }
    }
}
