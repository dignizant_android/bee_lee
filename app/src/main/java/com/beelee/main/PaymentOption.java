package com.beelee.main;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.beelee.App;
import com.beelee.Interface.ApiInterface;
import com.beelee.R;
import com.beelee.adpater.PaymentAdapter;
import com.beelee.constants.Api;
import com.beelee.constants.Constants;
import com.beelee.implementor.RecyclerViewItemClickListener;
import com.beelee.model.GsonMessage;
import com.beelee.model.PaymentData;
import com.beelee.utility.LogUtil;
import com.beelee.utility.MyUtils;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentOption extends AppCompatActivity implements View.OnClickListener, RecyclerViewItemClickListener {


    // note that these credentials will differ between live & sandbox
    // environments.
    public static final String CONFIG_CLIENT_ID = "AT6ShNigNXjOPlo8-hcZACyhu5gqGMT_uvbwhIf1rTQaZpZtU91GNgZozbSDpfLzkBcM3rw3NMOvJ_MJ";
    public static final String CONFIG_SECRET_CLIENT_ID = "EMRZpyxYzoTyxV3q-sJgcbWreD3sQX-pz9pZBd0p6tQ0a8HTW7knhdXNQ-RVLPs6LBQ7K_O8-B4lLI0g";
    private static final String TAG = "PaymentOption";
    private static final String LOGIN_URL = "https://api.fixer.io/latest?base=ILS";
    //paypal
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;
    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private static final int REQUEST_CODE_PROFILE_SHARING = 3;
    public static List<PaymentData> list = new ArrayList<>();
    public static String ACCESS_TOCKEN = "";
    public static String PAY_ID = "";
    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
            .merchantPrivacyPolicyUri(
                    Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(
                    Uri.parse("https://www.example.com/legal"));
    String finalrate;
    double USD;
    RecyclerView rv_payment;
    JSONObject otherDetailJson;
    ProgressBar cpb;
    TextView tv_title;
    ImageView iv_header_navigation;
    PaymentAdapter pAdp;
    String PAY_TYPE = "";
    int productPrice = 0;

    //Intent Data
    ArrayList<String> arr;
    String price, is_flag, product_id;
    private RecyclerView.LayoutManager mLayoutManager;
    StringBuilder sb = new StringBuilder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.RegisterActivityForBugsense(PaymentOption.this);
//        Window window = getWindow();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
//            window.setStatusBarColor(Color.TRANSPARENT);
////            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
//        }
        setContentView(R.layout.activity_payment_option);

        setupheaderview();
        findViewById();


        AddMenu();

        configurePaypal();
        main();
    }

    private void setupheaderview() {
        tv_title = findViewById(R.id.tv_title);
        iv_header_navigation = findViewById(R.id.iv_header_navigation);

        tv_title.setText(getResources().getString(R.string.text_payment));
        iv_header_navigation.setOnClickListener(this);
    }

    private void findViewById() {
        cpb = findViewById(R.id.cpb);
        rv_payment = findViewById(R.id.rv_payment);


    }

    private void AddMenu() {
        String[] menu = getResources().getStringArray(R.array.array_payment);
        if (list.size() > 0)
            list.clear();
        for (int i = 0; i < 2; i++) {

            PaymentData paymentData = new PaymentData();
            paymentData.setCategory(menu[i]);
            list.add(paymentData);
        }
        LogUtil.Print(TAG, "size==============" + list.size());


    }

    /**
     * configure paypal SDK
     */
    private void configurePaypal() {
//        config = config.acceptCreditCards(false);
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        this.startService(intent);
    }

    private void main() {
        mLayoutManager = new LinearLayoutManager(PaymentOption.this);
        rv_payment.setLayoutManager(mLayoutManager);
        pAdp = new PaymentAdapter(PaymentOption.this, list);
        pAdp.setOnRecyclerViewItemClickListener(this);
        rv_payment.setAdapter(pAdp);

        if (getIntent() != null) {
            is_flag = getIntent().getExtras().getString(Constants.is_flag);
            price = getIntent().getExtras().getString(Constants.price);

            if (is_flag.equals("1")) {
                Bundle b = getIntent().getExtras();

                if (null != b) {
                    arr = b.getStringArrayList(Constants.data);
                }

                String prefix = "";
                for (String str : arr) {
                    sb.append(prefix);
                    prefix = ",";
                    sb.append(str);
                }
                product_id = sb.toString();
            } else if (is_flag.equals("0")) {
                product_id = getIntent().getExtras().getString(Constants.product_id);
            }
        }

    }

    private void parseResult(String result) {
        try {


            JSONObject response = new JSONObject(result.toString());

            JSONObject ratesObject = response.getJSONObject("rates");

            USD = ratesObject.getDouble("USD");

            // double USD = ratesObject.getDouble("USD");


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * pay amount via paypal
     */
    private void makeChargePayPalAccount() {

        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);

    }
//PayPal

    private PayPalPayment getThingToBuy(String paymentIntent) {
        return new PayPalPayment(new BigDecimal(price), "ILS", "Total",
                paymentIntent);
    }

    private void makeChargeCreditCard() {
        PayPalPayment thingToBuyCredit = getThingToBuyCredit(PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuyCredit);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    private PayPalPayment getThingToBuyCredit(String paymentIntent) {
        return new PayPalPayment(new BigDecimal(finalrate), "USD", "Total",
                paymentIntent);
    }


    @Override
    public void onClick(View view) {
        if (view == iv_header_navigation) {
            onBackPressed();
            finish();
        }

    }


    @Override
    public void onItemClick(int position, int flag, View view) {
        if (flag == Constants.ITEM_CLICK) {
            if (position == 0) {
                PAY_TYPE = "1";
                makeChargePayPalAccount();
            } else if (position == 1) {
                PAY_TYPE = "2";
                new getRates().execute(LOGIN_URL);


//                MyUtils.makeToast("Credit Card Sevice Not Available");
            }
//            else if(position == 2)
//            {
//                PAY_TYPE = "3";
//                MyUtils.makeToast("Other Sevice Not Available");
//            }

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data
                        .getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        LogUtil.Print(TAG, confirm.toJSONObject().toString());
                        LogUtil.Print(TAG, confirm.getPayment().toJSONObject()
                                .toString(4));

                        JSONObject jo = confirm.toJSONObject();
                        LogUtil.Print("response", "..."
                                + jo.getJSONObject("response").getString("id"));
                        PAY_ID = jo.getJSONObject("response").getString("id");
                        Toast.makeText(
                                this,
                                "PaymentConfirmation info received from PayPal",
                                Toast.LENGTH_LONG).show();


                        completePurchaseAPIRequest(PAY_ID);

                    } catch (JSONException e) {
                        Log.e(TAG, "an extremely unlikely failure occurred: ",
                                e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                LogUtil.Print(TAG, "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                LogUtil.Print(TAG,
                        "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }


    }

    private void completePurchaseAPIRequest(String payId) {

        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.lang, Constants.language_en);
            params.put(Constants.user_id, App.Utils.getString(Constants.user_id));
            params.put(Constants.access_token, App.Utils.getString(Constants.access_token));
            params.put(Constants.product_id, product_id);
            params.put(Constants.transaction_id, payId);
            params.put(Constants.payment_type, PAY_TYPE);
            params.put(Constants.flag_view, is_flag);
            LogUtil.Print("Paypal_Payment_params", "" + params);


            Call<GsonMessage> call = request.addpaymentComplete(params);
            cpb.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<GsonMessage>() {
                @Override
                public void onResponse(Call<GsonMessage> call, Response<GsonMessage> response) {
                    cpb.setVisibility(View.GONE);
                    GsonMessage gson = response.body();

                    if (gson.getFlag().equals(Constants.RESPONSE_SUCCESS_FLAG)) {

                        MyUtils.makeToast("Payment Succesfully Complete");
                        Intent i = new Intent(PaymentOption.this, Home.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();

                    } else if (gson.getFlag().equals(Constants.RESPONSE_UNSUCCESS_FLAG)) {
                        App.Utils.makeToast(gson.getMsg());
                    }
                }

                @Override
                public void onFailure(Call<GsonMessage> call, Throwable t) {
                    Log.d("Error", t.getMessage());

                }
            });

        } else {
            App.Utils.makeToast(getResources().getString(R.string.text_internet));

        }

    }

    private class getRates extends AsyncTask<String, Void, Integer> {
        //ProgressDialog pdLoading = new ProgressDialog(getApplicationContext());
     /*   @Override
        protected void onPreExecute() {
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();
           *//* progressBar.setVisibility(View.VISIBLE);*//*
        }*/

        @Override
        protected Integer doInBackground(String... params) {
            Integer result = 0;
            HttpURLConnection urlConnection;
            try {
                URL url = new URL(params[0]);
                urlConnection = (HttpURLConnection) url.openConnection();
                int statusCode = urlConnection.getResponseCode();

                // 200 represents HTTP OK
                if (statusCode == 200) {
                    BufferedReader r = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder response = new StringBuilder();
                    String line;
                    while ((line = r.readLine()) != null) {
                        response.append(line);
                    }
                    parseResult(response.toString());
                    result = 1; // Successful
                } else {
                    result = 0; //"Failed to fetch data!";
                }
            } catch (Exception e) {
                LogUtil.Print(TAG, e.getLocalizedMessage());
            }
            return result; //"Failed to fetch data!";
        }

        @Override
        protected void onPostExecute(Integer result) {
            /*progressBar.setVisibility(View.GONE);*/
            //  pdLoading.dismiss();
            if (result == 1) {
                // pdLoading.dismiss();
                double rates = 0;
                rates = USD;
                // MyUtils.makeToast(""+rates);
                finalrate = String.valueOf(rates * Double.parseDouble(price));
                // MyUtils.makeToast(finalrate);
                makeChargeCreditCard();

            } else {
                Toast.makeText(getApplicationContext(), "Failed to fetch data!", Toast.LENGTH_SHORT).show();
            }
        }
    }


}
