package com.beelee.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.beelee.App;
import com.beelee.Interface.ApiInterface;
import com.beelee.R;
import com.beelee.adpater.ProductDetailAdapter;
import com.beelee.constants.Api;
import com.beelee.constants.Constants;
import com.beelee.implementor.RecyclerViewItemClickListener;
import com.beelee.model.DataDetailProduct;
import com.beelee.model.GsonDetailProduct;
import com.beelee.utility.LogUtil;
import com.beelee.utility.NavigatorManager;
import com.google.gson.Gson;
import com.paginate.Paginate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailsProduct extends AppCompatActivity implements View.OnClickListener, RecyclerViewItemClickListener {

    public static List<DataDetailProduct> list = new ArrayList<>();
    //header
    ImageView iv_header_navigation;
    TextView tv_title;
    TextView tv_pay, tv_submit, tv_err_msg, tv_total;
    RecyclerView rv_product;
    ProgressBar cpb;
    String offset = "0";
    Paginate paginate;
    ProductDetailAdapter dAdp;
    private boolean isLoading = false;
    private RecyclerView.LayoutManager mLayoutManager;
    String total;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        setupheaderview();
        findViewById();
        main();
        setUpPagination();
    }

    /*set Header*/
    private void setUpPagination() {

        if (paginate != null) {

            paginate.unbind();
        }

        paginate = Paginate.with(rv_product, new Paginate.Callbacks() {
            @Override
            public void onLoadMore() {
                isLoading = true;

                getProductDetailListAPI();

            }


            @Override
            public boolean isLoading() {
                return isLoading;
            }

            @Override
            public boolean hasLoadedAllItems() {

                if (offset.equals(Constants.pagination_last_offset))
                    return true;
                else
                    return false;
            }
        }).setLoadingTriggerThreshold(2)
                .addLoadingListItem(true)
                .build();
    }


    private void setupheaderview() {
        iv_header_navigation = findViewById(R.id.iv_header_navigation);
        tv_title = findViewById(R.id.tv_title);
        tv_title.setText(getResources().getString(R.string.text_detail));
        iv_header_navigation.setOnClickListener(this);

    }

    private void findViewById() {
        cpb = findViewById(R.id.cpb);
        rv_product = findViewById(R.id.rv_product);
        tv_total = findViewById(R.id.tv_total);
        tv_err_msg = findViewById(R.id.tv_err_msg);
        tv_pay = findViewById(R.id.tv_pay);
        tv_submit = findViewById(R.id.tv_submit);
        tv_submit.setVisibility(View.GONE);
        tv_pay.setVisibility(View.VISIBLE);
        tv_pay.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == iv_header_navigation) {
            onBackPressed();
            finish();
        } else if (view == tv_pay) {
            final ArrayList<String> arr = new ArrayList<>();
            for (int i = 0; i < list.size() ; i++) {
                arr.add(list.get(i).getBuyerproductId());

            }
            Intent intent = new Intent(this, PaymentOption.class);
            intent.putExtra(Constants.data, arr);
            intent.putExtra(Constants.price, total);
            intent.putExtra(Constants.is_flag, getIntent().getExtras().getString(Constants.is_flag));
//            intent.putExtra(Constants.detail, 1);
            NavigatorManager.startNewActivity(DetailsProduct.this, intent);
        }

    }

    private void main() {
        mLayoutManager = new LinearLayoutManager(DetailsProduct.this);
        rv_product.setLayoutManager(mLayoutManager);
        dAdp = new ProductDetailAdapter(DetailsProduct.this, list);
        dAdp.setOnRecyclerViewItemClickListener(this);
        rv_product.setAdapter(dAdp);
    }

    private void getProductDetailListAPI() {
        if (App.Utils.IsInternetOn()) {
            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.user_id, App.Utils.getString(Constants.user_id));
            params.put(Constants.access_token, App.Utils.getString(Constants.access_token));
            params.put(Constants.lang, Constants.language_en);
            params.put(Constants.seller_id, getIntent().getExtras().getString(Constants.seller_id));
            params.put(Constants.product_id, getIntent().getExtras().getString(Constants.product_id));
            params.put(Constants.category_id, getIntent().getExtras().getString(Constants.category_id));
            params.put(Constants.offset, offset);
            LogUtil.Print("My Product Detail List ApiRequest params", "" + params);
            Call<GsonDetailProduct> call = request.getProductDetails(params);

            if (list.size() == 0) {
                cpb.setVisibility(View.VISIBLE);
            }

            call.enqueue(new Callback<GsonDetailProduct>() {
                @Override
                public void onResponse(Call<GsonDetailProduct> call, Response<GsonDetailProduct> response) {
                    GsonDetailProduct gsonProductList = response.body();
                    if (gsonProductList.getFlag() == Constants.RESPONSE_SUCCESS_FLAG) {
                        tv_err_msg.setVisibility(View.GONE);
                        if (list.size() == 0)
                            cpb.setVisibility(View.GONE);
                        if (list.size() > 0)
                            list.clear();

                        tv_total.setText(getResources().getString(R.string.ils) + " " +gsonProductList.getTotalPrice());
                        total = gsonProductList.getTotalPrice();
                        offset = "" + gsonProductList.getNextOffset();
                        list.addAll(gsonProductList.getData());

                        dAdp.notifyDataSetChanged();
                        isLoading = false;

                        if (paginate != null) {
                            if (offset.equals(Constants.pagination_last_offset))
                                paginate.setHasMoreDataToLoad(false);
                            else paginate.setHasMoreDataToLoad(true);
                        }

                    } else {
                        cpb.setVisibility(View.GONE);
                        tv_err_msg.setVisibility(View.VISIBLE);
                        tv_err_msg.setText(gsonProductList.getMsg());
//                        tv_err_msg.setText("No Vendor Found");
                        if (list.size() > 0) {
                            list.clear();
                            dAdp.notifyDataSetChanged();
                        }
                        isLoading = false;
                        if (paginate != null)
                            paginate.setHasMoreDataToLoad(false);
                    }
                }

                @Override
                public void onFailure(Call<GsonDetailProduct> call, Throwable t) {
                    if (list.size() == 0)
                        cpb.setVisibility(View.GONE);
                    LogUtil.Print("===Product List Error====", t.getMessage());
                }
            });

        } else {
            App.Utils.makeSnackbar(rv_product, getResources().getString(R.string.text_internet));

        }

    }

    @Override
    public void onItemClick(int position, int flag, View view) {

    }
}
