package com.beelee.main;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.ViewGroup;

import com.beelee.App;
import com.beelee.Interface.ApiInterface;
import com.beelee.R;
import com.beelee.constants.Api;
import com.beelee.constants.Constants;
import com.beelee.model.GsonScanner;
import com.beelee.qrscanner.BaseScannerActivity;
import com.beelee.qrscanner.CustomViewFinderView;
import com.beelee.utility.LogUtil;
import com.beelee.utility.MyUtils;
import com.beelee.utility.NavigatorManager;
import com.google.zxing.Result;

import java.util.HashMap;
import java.util.Map;

import me.dm7.barcodescanner.core.IViewFinder;
import me.dm7.barcodescanner.core.ViewFinderView;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Scanner extends BaseScannerActivity implements ZXingScannerView.ResultHandler {
    String qrcode;
    private ZXingScannerView mScannerView;

    private void chkPermission() {

        if (!(App.Utils.checkPermission(Scanner.this, android.Manifest.permission.CAMERA))) {

            App.Utils.showPermissionsDialog(Scanner.this, new String[]{android.Manifest.permission.CAMERA},
                    Constants.REQUEST_CODE_CAMERA);
            return;
        }
    }

    /**
     * check permission
     */


    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        App.RegisterActivityForBugsense(Scanner.this);
        chkPermission();
        setContentView(R.layout.activity_scanner);
        setupToolbar();


        ViewGroup contentFrame = (ViewGroup) findViewById(R.id.content_frame);
        mScannerView = new ZXingScannerView(this){
            @Override
            protected IViewFinder createViewFinderView(Context context) {
                return new CustomViewFinderView(context);
            }
        };
        //mScannerView.setBorderStrokeWidth();
        contentFrame.addView(mScannerView);


        //GetNotification();


    }


   /* private void GetNotification() {
        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            String jsonString = intent.getStringExtra(Constants.push_message);
            msg = intent.getStringExtra(Constants.message);
            if (msg != null) {
                try {
                    custom_data = new JSONObject(jsonString);
                    product_id = custom_data.getString("product_id");
                    price = custom_data.getString("price");
                    comment = custom_data.getString("message");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.e("custom_data", "" + custom_data.toString());
                Log.e("msg", "" + msg);
            }
        }
        if (custom_data != null) {

        }

    }*/


    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
        qrcode = rawResult.getText();
        if (qrcode.matches("^[a-zA-Z0-9].*-.*[a-zA-Z0-9]$")) {
            LogUtil.Print("Match", "QR Match");
            if (!qrcode.equals("")) {
                String[] strings = qrcode.split("-");

                String sellerid;
                String productcode;

                sellerid = strings[0];
                productcode = strings[1];
                if (getIntent().getExtras().getInt(Constants.data) == Constants.homePay)
//                    MyUtils.makeToast("Pay");
                    CallScanPayApi(sellerid, productcode);
                else if (getIntent().getExtras().getInt(Constants.data) == Constants.homeDetail) {
                    Intent i = new Intent(Scanner.this, AllProductList.class);
                    i.putExtra(Constants.seller_id, sellerid);
                    NavigatorManager.startNewActivity(Scanner.this, i);
                }


            }
        } else {
            LogUtil.Print("Match", "QR Not Match");
            AlertDialog.Builder builder = new AlertDialog.Builder(Scanner.this);
            builder.setTitle(R.string.app_name);
            builder.setMessage(R.string.text_wrong_qr_code)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.text_ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //car_poolig_id = list.get(position).getCarpooling_details_id();
                            dialog.cancel();
                        }
                    });

            AlertDialog alert = builder.create();
            alert.show();
        }
        //MyUtils.makeToast("Content:--"+rawResult.getText());


        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mScannerView.resumeCameraPreview(Scanner.this);
            }
        }, 2000);
    }

    private void CallScanPayApi(String sellerid, final String productcode) {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.lang, Constants.language_en);
            params.put(Constants.user_id, App.Utils.getString(Constants.user_id));
            params.put(Constants.access_token, App.Utils.getString(Constants.access_token));
            params.put(Constants.product_code, productcode);
            params.put(Constants.seller_id, sellerid);
            LogUtil.Print("Scanner_params", "" + params);


            Call<GsonScanner> call = request.scanqrcode(params);
            call.enqueue(new Callback<GsonScanner>() {
                @Override
                public void onResponse(Call<GsonScanner> call, Response<GsonScanner> response) {
                    GsonScanner gson = response.body();

                    if (gson.getFlag().equals(Constants.RESPONSE_SUCCESS_FLAG)) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(Scanner.this);
                        builder.setTitle(R.string.app_name);
                        builder.setMessage(R.string.text_push)
                                .setCancelable(false)
                                .setPositiveButton(getResources().getString(R.string.text_ok), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //car_poolig_id = list.get(position).getCarpooling_details_id();
                                        dialog.cancel();
                                        finish();
                                    }
                                });

                        AlertDialog alert = builder.create();
                        alert.show();

                    } else if (gson.getFlag().equals(Constants.RESPONSE_UNSUCCESS_FLAG)) {
                        App.Utils.makeToast(gson.getMsg());
                    }
                }

                @Override
                public void onFailure(Call<GsonScanner> call, Throwable t) {
                    Log.d("Error", t.getMessage());

                }
            });

        } else {
            App.Utils.makeToast(getResources().getString(R.string.text_internet));

        }
    }




}

