package com.beelee;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.beelee.constants.Constants;
import com.beelee.main.AllProductList;
import com.beelee.main.Home;
import com.beelee.main.ResultProduct;
import com.beelee.main.Scanner;

public class Splash extends AppCompatActivity {
    private static final String TAG = "Splash";

    private static int SPLASH_WAIT = 3000;
    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.RegisterActivityForBugsense(Splash.this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        BGThread();
    }

    private void BGThread() {

        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
//                Intent i = new Intent(Splash.this, LanguageSelect.class);
//                startActivity(i);
                if ((App.Utils.getString(Constants.user_id)).equalsIgnoreCase("")) {
                    Intent i1 = new Intent(Splash.this, Login.class);
                    startActivity(i1);
                    finish();
                }
                else
                {
                    Intent i1 = new Intent(Splash.this, Home.class);
                    startActivity(i1);
                    finish();
                }

//
//                if (App.Utils.getString(Constants.user_id).equalsIgnoreCase("")) {
//
//                    Intent i = new Intent(Splash.this, Home.class);
//                    startActivity(i);
//
//                }
                finish();
            }

        }, SPLASH_WAIT);
    }
}
