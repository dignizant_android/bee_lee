package com.beelee;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.beelee.Interface.ApiInterface;
import com.beelee.constants.Api;
import com.beelee.constants.Constants;
import com.beelee.main.Home;
import com.beelee.model.GsonMessage;
import com.beelee.model.GsonOtp;
import com.beelee.model.GsonRegister;
import com.beelee.utility.LogUtil;
import com.beelee.utility.MyUtils;
import com.beelee.utility.NavigatorManager;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Verify_otp extends AppCompatActivity implements View.OnClickListener {

    TextView tv_reset;
    ImageView iv_back;
    EditText et_otp;
    Button btn_check_otp;
    ProgressBar cpb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.RegisterActivityForBugsense(Verify_otp.this);
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            window.setStatusBarColor(Color.TRANSPARENT);
//            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        setContentView(R.layout.activity_verify_otp);

//        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                CallgetOtpAPI();
//                   }
//        }, 1000);
        findViewById();
        main();
    }

    private void findViewById() {
        tv_reset = findViewById(R.id.tv_reset);
        iv_back = findViewById(R.id.iv_back);
        et_otp = findViewById(R.id.et_otp);
        btn_check_otp = findViewById(R.id.btn_check_otp);
        cpb = findViewById(R.id.cpb);

    }

    private void main() {
        tv_reset.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        btn_check_otp.setOnClickListener(this);

        //CallgetOtpAPI();
    }

    private void CallgetOtpAPI() {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.lang, Constants.language_en);
            params.put(Constants.mobile_no, getIntent().getExtras().getString(Constants.mobile_no));

            LogUtil.Print("Get_otp_params", "" + params);


            Call<GsonOtp> call = request.getOtpCode(params);
            cpb.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<GsonOtp>() {
                @Override
                public void onResponse(Call<GsonOtp> call, Response<GsonOtp> response) {
                    cpb.setVisibility(View.GONE);
                    GsonOtp gson = response.body();

                    et_otp.setText(gson.getOtpCode());

                }

                @Override
                public void onFailure(Call<GsonOtp> call, Throwable t) {
                    Log.d("Error", t.getMessage());

                }
            });

        } else {
            App.Utils.makeSnackbar(et_otp, getResources().getString(R.string.text_internet));

        }
    }

    @Override
    public void onClick(View view) {
        if (view == iv_back) {
            finish();
        }
        if (view == tv_reset) {
            CallResendotpAPI();
        }
        if (view == btn_check_otp) {
            if (et_otp.getText().toString().isEmpty()) {
                et_otp.requestFocus();
                MyUtils.makeSnackbar(et_otp, getResources().getString(R.string.text_err_otp));
            } else {
                CallCheckOtpApi();
            }
        }
    }


    private void CallCheckOtpApi() {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.otp_code, et_otp.getText().toString());
            params.put(Constants.lang, Constants.language_en);
            params.put(Constants.user_id, getIntent().getExtras().getString(Constants.user_id));

            LogUtil.Print("Verify_otp_params", "" + params);


            Call<GsonRegister> call = request.getUserOtp(params);
            cpb.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<GsonRegister>() {
                @Override
                public void onResponse(Call<GsonRegister> call, Response<GsonRegister> response) {
                    cpb.setVisibility(View.GONE);
                    GsonRegister gson = response.body();

                    if (gson.getFlag().equals(Constants.RESPONSE_SUCCESS_FLAG)) {

                        App.Utils.makeSnackbar(et_otp, gson.getMsg());
                        App.Utils.putString(Constants.user_id, gson.getData().getUser_id());
                        App.Utils.putString(Constants.access_token, gson.getData().getAccess_token());
//                        App.Utils.putString(Constants.access_token, gson.getData().getAccess_token());
//                        App.Utils.putString(Constants.mobile_no, gson.getData().getMobile_no());
//                        String devicetoken;
//                        devicetoken = App.Utils.getString(Constants.device_token);
//
//
//                        MyUtils.ClearAllPreferences();
//
//                        App.Utils.putString(Constants.device_token, devicetoken);

                        NavigatorManager.startNewActivity(Verify_otp.this, new Intent(Verify_otp.this, Home.class));
                        finish();

                    } else if (gson.getFlag().equals(Constants.RESPONSE_UNSUCCESS_FLAG)) {
                        App.Utils.makeSnackbar(et_otp, gson.getMsg());
                    }
                }

                @Override
                public void onFailure(Call<GsonRegister> call, Throwable t) {
                    Log.d("Error", t.getMessage());

                }
            });

        } else {
            App.Utils.makeSnackbar(et_otp, getResources().getString(R.string.text_internet));

        }
    }

    private void CallResendotpAPI() {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.lang, Constants.language_en);
            params.put(Constants.mobile_no, getIntent().getExtras().getString(Constants.mobile_no));

            LogUtil.Print("Resend_otp_params", "" + params);


            Call<GsonMessage> call = request.resendOtp(params);
            cpb.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<GsonMessage>() {
                @Override
                public void onResponse(Call<GsonMessage> call, Response<GsonMessage> response) {
                    cpb.setVisibility(View.GONE);
                    GsonMessage gson = response.body();

                    if (gson.getFlag().equals(Constants.RESPONSE_SUCCESS_FLAG)) {

                        App.Utils.makeSnackbar(et_otp, gson.getMsg());
                        // CallgetOtpAPI();


                    } else if (gson.getFlag().equals(Constants.RESPONSE_UNSUCCESS_FLAG)) {
                        App.Utils.makeSnackbar(et_otp, gson.getMsg());
                    }
                }

                @Override
                public void onFailure(Call<GsonMessage> call, Throwable t) {
                    Log.d("Error", t.getMessage());

                }
            });

        } else {
            App.Utils.makeSnackbar(et_otp, getResources().getString(R.string.text_internet));

        }
    }
}
