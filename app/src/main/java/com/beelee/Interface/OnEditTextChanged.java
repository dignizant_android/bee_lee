package com.beelee.Interface;

/**
 * Created by haresh on 2/12/18.
 */

public interface OnEditTextChanged {
    void onTextChanged(int position, String charSeq);
}