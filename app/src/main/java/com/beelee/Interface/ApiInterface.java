package com.beelee.Interface;

import com.beelee.model.GsonAddProductQuantity;
import com.beelee.model.GsonCategory;
import com.beelee.model.GsonCounrtyList;
import com.beelee.model.GsonDetailProduct;
import com.beelee.model.GsonMessage;
import com.beelee.model.GsonOtp;
import com.beelee.model.GsonProductList;
import com.beelee.model.GsonRegister;
import com.beelee.model.GsonScanner;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by siddharth on 12/13/2017.
 */
public interface ApiInterface {

    @FormUrlEncoded
    @POST("user/country_list")
    Call<GsonCounrtyList> getCountryList(@FieldMap Map<String, String> user);


    @FormUrlEncoded
    @POST("user/user_register")
    Call<GsonRegister> getUserRegister(@FieldMap Map<String, String> user);


    @FormUrlEncoded
    @POST("user/check_otp_code")
    Call<GsonRegister> getUserOtp(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("user/get_otp")
    Call<GsonOtp> getOtpCode(@FieldMap Map<String, String> user);


    @FormUrlEncoded
    @POST("user/resend_otp")
    Call<GsonMessage> resendOtp(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("user/user_login")
    Call<GsonRegister> getuserLogin(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("user/user_logout")
    Call<GsonMessage> logoutuser(@FieldMap Map<String, String> user);


    @FormUrlEncoded
    @POST("user/scan_qr_code")
    Call<GsonScanner> scanqrcode(@FieldMap Map<String, String> user);


    @FormUrlEncoded
    @POST("user/product_details")
    Call<GsonScanner> productDetail(@FieldMap Map<String, String> user);


    @FormUrlEncoded
    @POST("user/add_paypal_payment")
    Call<GsonMessage> addpaymentComplete(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("user/product_list")
    Call<GsonProductList> getProductList(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("user/add_product_quantity")
    Call<GsonAddProductQuantity> setProductQuantity(@FieldMap Map<String, String> user);


    @FormUrlEncoded
    @POST("user/buyer_product_details")
    Call<GsonDetailProduct> getProductDetails(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("user/category_list")
    Call<GsonCategory> getCategoryList(@FieldMap Map<String, String> user);
}
