package com.beelee.utility;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.io.File;


public class ImageLoaderUtils {

    public static void loadImageFromURLWithoutResize(Context context, String imgUrl, ImageView iv, int placeholderResourceID, @Nullable Transformation transformation) {
        try {
            if (imgUrl.isEmpty()) {
                iv.setImageResource(placeholderResourceID);
            } else {
                if (transformation != null) {
                    Picasso.with(context)
                            .load(imgUrl)
                            .placeholder(placeholderResourceID)
                            .transform(transformation)
                            .into(iv);
                } else {
                    Picasso.with(context)
                            .load(imgUrl)
                            .placeholder(placeholderResourceID)
                            .into(iv);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void loadImageFromURL(Context context, String imgUrl, ImageView iv, int placeholderResourceID, @Nullable Transformation transformation, int resizeWidth, int resizeHeight) {
        try {
            if (imgUrl.isEmpty()) {
                iv.setImageResource(placeholderResourceID);
            } else {
                if (transformation != null) {
                    Picasso.with(context)
                            .load(imgUrl)
                            .placeholder(placeholderResourceID)
                            .transform(transformation)
                            .resizeDimen(resizeWidth, resizeHeight)
                            .centerCrop()
                            .into(iv);
                } else {
                    Picasso.with(context)
                            .load(imgUrl)
                            .placeholder(placeholderResourceID)
                            .resizeDimen(resizeWidth, resizeHeight)
                            .centerCrop()
                            .into(iv);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void loadImageFromURLForClassicView(Context context, String imgUrl, ImageView iv, int placeholderResourceID, @Nullable Transformation transformation, int resizeWidth, int resizeHeight) {
        try {
            if (imgUrl.isEmpty()) {
                iv.setImageResource(placeholderResourceID);
            } else {
                if (transformation != null) {
                    Picasso.with(context)
                            .load(imgUrl)
                            .placeholder(placeholderResourceID)
                            .transform(transformation)
                            .resize(resizeWidth, resizeHeight)
                            .centerCrop()
                            .into(iv);
                } else {
                    Picasso.with(context)
                            .load(imgUrl)
                            .placeholder(placeholderResourceID)
                            .resize(resizeWidth, resizeHeight)
                            .centerCrop()
                            .into(iv);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void loadImageFromURLWithListener(Context context, String imgUrl, ImageView iv, int placeholderResourceID, @Nullable Transformation transformation, final Callback callback, int resizeWidth, int resizeHeight) {
        try {
            if (imgUrl.isEmpty()) {
                iv.setImageResource(placeholderResourceID);
            } else {
                if (transformation != null) {
                    Picasso.with(context)
                            .load(imgUrl)
                            .placeholder(placeholderResourceID)
                            .transform(transformation)
                            .into(iv, new Callback() {
                                @Override
                                public void onSuccess() {
                                    callback.onSuccess();
                                }

                                @Override
                                public void onError() {
                                    callback.onError();
                                }
                            });
                } else {
                    Picasso.with(context)
                            .load(imgUrl)
                            .placeholder(placeholderResourceID)
                            .into(iv, new Callback() {
                                @Override
                                public void onSuccess() {
                                    if (callback != null)
                                        callback.onSuccess();
                                }

                                @Override
                                public void onError() {

                                    if (callback != null) callback.onError();
                                }
                            });
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void loadImageFromDrawable(Context context, int imgResourceID, ImageView iv, int placeholderResourceID, @Nullable Transformation transformation) {
        try {
            if (imgResourceID == 0) {
                iv.setImageResource(placeholderResourceID);
            } else {
                if (transformation != null) {
                    Picasso.with(context)
                            .load(imgResourceID)
                            .placeholder(placeholderResourceID)
                            .transform(transformation)
                            .into(iv);
                } else {
                    Picasso.with(context)
                            .load(imgResourceID)
                            .placeholder(placeholderResourceID)
                            .into(iv);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void loadImageFromPath(Context context, String imgPath, ImageView iv, int placeholderResourceID, @Nullable Transformation transformation, int resizeWidth, int resizeHeight) {
        try {
            if (imgPath.equalsIgnoreCase("")) {
                iv.setImageResource(placeholderResourceID);
            } else {
                Uri uri = Uri.fromFile(new File(imgPath));
                if (transformation != null) {
                    Picasso.with(context)
                            .load(uri)
                            .placeholder(placeholderResourceID)
                            .transform(transformation)
                            .resizeDimen(resizeWidth, resizeHeight)
                            .centerCrop()
                            .into(iv);
                } else {
                    Picasso.with(context)
                            .load(uri)
                            .placeholder(placeholderResourceID)
                            .into(iv);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void loadImageFromFile(Context context, File imgFile, ImageView iv, int placeholderResourceID, @Nullable Transformation transformation, int resizeWidth, int resizeHeight) {
        try {
            if (imgFile == null) {
                iv.setImageResource(placeholderResourceID);
            } else {
                if (transformation != null) {
                    Picasso.with(context)
                            .load(imgFile)
                            .placeholder(placeholderResourceID)
                            .transform(transformation)
                            .resizeDimen(resizeWidth, resizeHeight)
                            .centerCrop()
                            .into(iv);
                } else {
                    Picasso.with(context)
                            .load(imgFile)
                            .placeholder(placeholderResourceID)
                            .into(iv);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
