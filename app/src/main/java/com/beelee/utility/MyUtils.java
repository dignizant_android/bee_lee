package com.beelee.utility;


import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.ResultReceiver;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.beelee.R;
import com.beelee.constants.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class MyUtils {

    public static final String NUMBERS_AND_LETTERS = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static final String NUMBERS = "0123456789";
    public static final String LETTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static final String CAPITAL_LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static final String LOWER_CASE_LETTERS = "abcdefghijklmnopqrstuvwxyz";
    public static String PREFERENCE_NAME = "GLOBAL_DATA";

    private static final char[] DIGITS_LOWER = {'0', '1', '2', '3', '4', '5',
            '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    static Context ctx;

    public static ProgressDialog pDialog = null, pDialogSimilor = null, pDialogTournament = null ,pDialogSecond = null;

    public MyUtils(Context ctx) {
        super();
        this.ctx = ctx;
    }

    // -------------------------------------------------//
    // ----------------Show Toast-----------------------//
    // -------------------------------------------------//

    public static void makeToast(String text) {
        Toast.makeText(ctx, text, Toast.LENGTH_SHORT).show();
    }

    // -------------------------------------------------//
    // -----------------Share Preference---------------//
    // -------------------------------------------------//

    public static boolean putString(String key, String value) {
        SharedPreferences settings = ctx.getSharedPreferences(PREFERENCE_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        return editor.commit();
    }

    public static String getString(String key) {
        return getString(ctx, key, "");
    }

    public static String getLanguageString(String key) {
        return getString(ctx, key, "" + Constants.language_en);
    }

    public static String getString(Context context, String key,
                                   String defaultValue) {
        SharedPreferences settings = context.getSharedPreferences(
                PREFERENCE_NAME, Context.MODE_PRIVATE);
        return settings.getString(key, defaultValue);
    }

    public static boolean putInt(String key, int value) {
        SharedPreferences settings = ctx.getSharedPreferences(PREFERENCE_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(key, value);
        return editor.commit();
    }

    public static int getInt(String key) {
        return getInt(ctx, key, -1);
    }

    public static int getInt(Context context, String key, int defaultValue) {
        SharedPreferences settings = context.getSharedPreferences(
                PREFERENCE_NAME, Context.MODE_PRIVATE);
        return settings.getInt(key, defaultValue);
    }

    public static boolean putLong(String key, long value) {
        SharedPreferences settings = ctx.getSharedPreferences(PREFERENCE_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong(key, value);
        return editor.commit();
    }

    public static long getLong(String key) {
        return getLong(ctx, key, -1);
    }

    public static long getLong(Context context, String key, long defaultValue) {
        SharedPreferences settings = context.getSharedPreferences(
                PREFERENCE_NAME, Context.MODE_PRIVATE);
        return settings.getLong(key, defaultValue);
    }

    public static boolean putFloat(String key, float value) {
        SharedPreferences settings = ctx.getSharedPreferences(PREFERENCE_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putFloat(key, value);
        return editor.commit();
    }

    public static float getFloat(String key) {
        return getFloat(ctx, key, -1);
    }

    public static float getFloat(Context context, String key, float defaultValue) {
        SharedPreferences settings = context.getSharedPreferences(
                PREFERENCE_NAME, Context.MODE_PRIVATE);
        return settings.getFloat(key, defaultValue);
    }

    public static boolean putBoolean(String key, boolean value) {
        SharedPreferences settings = ctx.getSharedPreferences(PREFERENCE_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(key, value);
        return editor.commit();
    }

    public static boolean getBoolean(String key) {
        return getBoolean(ctx, key, false);
    }

    public static boolean getBoolean(Context context, String key,
                                     boolean defaultValue) {
        SharedPreferences settings = context.getSharedPreferences(
                PREFERENCE_NAME, Context.MODE_PRIVATE);
        return settings.getBoolean(key, defaultValue);
    }

    // -------------------------------------------------//
    // --------internet coonection states---------------//
    // -------------------------------------------------//
    public final boolean IsInternetOn() {

        boolean connected = false;
        final ConnectivityManager cm = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            connected = true;
        } else if (netInfo != null && netInfo.isConnected()
                && cm.getActiveNetworkInfo().isAvailable()) {
            connected = true;
        } else if (netInfo != null && netInfo.isConnected()) {
            try {
                URL url = new URL("http://www.google.com");
                HttpURLConnection urlc = (HttpURLConnection) url
                        .openConnection();
                urlc.setConnectTimeout(3000);
                urlc.connect();
                if (urlc.getResponseCode() == 200) {
                    connected = true;
                }
            } catch (MalformedURLException e1) {
                e1.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (cm != null) {
            final NetworkInfo[] netInfoAll = cm.getAllNetworkInfo();
            for (NetworkInfo ni : netInfoAll) {
                System.out.println("get network type :::" + ni.getTypeName());
                if ((ni.getTypeName().equalsIgnoreCase("WIFI") || ni
                        .getTypeName().equalsIgnoreCase("MOBILE"))
                        && ni.isConnected() && ni.isAvailable()) {
                    connected = true;
                    if (connected) {
                        break;
                    }
                }
            }
        }
        return connected;

    }
    // -------------------------------------------------//
    // ----------------Show Snackbar-----------------------//
    // -------------------------------------------------//

    public static void makeSnackbar(View view, String text) {
        if (view != null) {
            Snackbar snackbar = Snackbar
                    .make(view, text, Snackbar.LENGTH_LONG);

            snackbar.show();
        }
    }
    // -------------------------------------------------//
    // --------IS GPS on states---------------//
    // -------------------------------------------------//
    public boolean isGPSEnabled() {
        LocationManager locationManager = (LocationManager) ctx
                .getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    // -------------------------------------------------//
    // --------email address validation-----------------//
    // -------------------------------------------------//
    public static boolean IsValidEmail(String email2) {
        boolean check;
        Pattern p;
        Matcher m;

        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        p = Pattern.compile(EMAIL_STRING);

        m = p.matcher(email2);
        check = m.matches();

        return check;
    }

    // -------------------------------------------------//
    // --------phone number validation-----------------//
    // -------------------------------------------------//
    public boolean IsValidPhoneNumber(String phone) {
        boolean check;
        Pattern p;
        Matcher m;

        String PHONE_STRING = "^[+]?[0-9]{10,13}$";

        p = Pattern.compile(PHONE_STRING);

        m = p.matcher(phone);
        check = m.matches();

        return check;
    }

    // -------------------------------------------------//
    // --------password >6 char validation--------------//
    // -------------------------------------------------//
    @SuppressWarnings("unused")
    private boolean IsValidPassword(String pass) {
        if (pass != null && pass.length() > 6) {
            return true;
        }
        return false;
    }

    // -------------------------------------------------//
    // --------min no of char validation--------------//
    // -------------------------------------------------//
    @SuppressWarnings("unused")
    private boolean IsMinChar(String text, int min) {
        if (text != null && text.length() >= min) {
            return true;
        }
        return false;
    }

    // -------------------------------------------------//
    // --------max no of char validation--------------//
    // -------------------------------------------------//
    @SuppressWarnings("unused")
    private boolean IsMaxChar(String text, int max) {
        if (text != null && text.length() <= max) {
            return true;
        }
        return false;
    }

    // -------------------------------------------------//
    // --------edittext empty or not validation---------//
    // -------------------------------------------------//
    @SuppressWarnings("unused")
    private boolean IsEmpty(EditText et) {
        if (et.getText().toString().equalsIgnoreCase("")) {
            return true;
        }
        return false;
    }

    // -------------------------------------------------//
    // --------removeLastChar----------------------------//
    // -------------------------------------------------//
    public static String removeLastChar(String str) {
        if (str != null && str.length() > 0 && str.charAt(str.length() - 1) == ',') {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }

    // -------------------------------------------------//
    // --------showRandomInteger------------------------//
    // -------------------------------------------------//
    public static int showRandomInteger(int aStart, int aEnd, Random aRandom) {
        if (aStart > aEnd) {
            throw new IllegalArgumentException("Start cannot exceed End.");
        }
        // get the range, casting to long to avoid overflow problems
        long range = (long) aEnd - (long) aStart + 1;
        // compute a fraction of the range, 0 <= frac < range
        long fraction = (long) (range * aRandom.nextDouble());
        int randomNumber = (int) (fraction + aStart);
        return randomNumber;
    }

    // -------------------------------------------------//
    // --------hide keyboard----------------------------//
    // -------------------------------------------------//
    public static void hideKeyBoard(EditText editText) {
        InputMethodManager inputMethodManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager
                .hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }


    // -------------------------------------------------//
    // --------ChangeFocus of keyboard---------------//
    // -------------------------------------------------//

    public static void ChangeKeyboardFocus(EditText editText) {
        editText.requestFocus();
        InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }

    // -------------------------------------------------//
    // --------getStorageDirectory----------------------//
    // -------------------------------------------------//
    public static String getStorageDirectory() {
        Boolean isSDPresent = Environment.getExternalStorageState()
                .equals(Environment.MEDIA_MOUNTED);

        String filename;

        if (isSDPresent) {
            // yes SD-card is present

            filename = Environment.getExternalStorageDirectory().getPath();

        } else {
            filename = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES).getPath();

        }

        return filename;

    }

    // -------------------------------------------------//
    // --------getRealPathFromURI----------------------//
    // -------------------------------------------------//
    public static String getRealPathFromURI(Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = ctx.getContentResolver().query(contentUri, proj, null,
                    null, null);
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    // -------------------------------------------------//
    // --------getRealPathFromURI----------------------//
    // -------------------------------------------------//
    @SuppressWarnings("deprecation")
    public String getRealPathFromURI(Uri contentUri, Activity activity) {
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            Cursor cursor = activity.managedQuery(contentUri, proj, null,
                    null, null);
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (Exception e) {
            return contentUri.getPath();
        }
    }

    // -------------------------------------------------//
    // ------------date conversion----------------------//
    // -------------------------------------------------//

    public static String GetDateOnRequireFormat(String date,
                                                String givenformat, String resultformat) {
        String result = "";
        SimpleDateFormat sdf;
        SimpleDateFormat sdf1;
        try {
            sdf = new SimpleDateFormat(givenformat, Locale.US);
            sdf1 = new SimpleDateFormat(resultformat, Locale.US);
            result = sdf1.format(sdf.parse(date));
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        } finally {
            sdf = null;
            sdf1 = null;
        }
        return result;
    }

    // -------------------------------------------------//
    // ------------adding days to given date-------------//
    // -------------------------------------------------//

    public static Date addDays(Date date, int days) {
        Calendar cal = Calendar.getInstance(Locale.US);
        cal.setTime(date);
        cal.add(Calendar.DATE, days); //minus number would decrement the days
        return cal.getTime();
    }
    // -------------------------------------------------//
    // ------------adding month to given date-------------//
    // -------------------------------------------------//

    public static Date addMonths(Date date, int months) {
        Calendar cal = Calendar.getInstance(Locale.US);
        cal.setTime(date);
        cal.add(Calendar.MONTH, months); //minus number would decrement the months
        return cal.getTime();
    }
    // -------------------------------------------------//
    // ------------date conversion----------------------//
    // -------------------------------------------------//

    public static String GetTitleDate(int flag) {
        String result = "";

        if (flag == 0) {
            result = GetDateFromTimeStamp(GetCurrentTimeStamp(), Constants.DATE_YYYY_MM_DD_FORMAT);

        } else if (flag == 1) {
            Calendar c = Calendar.getInstance(Locale.ENGLISH);
            c.add(Calendar.DATE, 1);
            Date dt = c.getTime();
            SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_YYYY_MM_DD_FORMAT);
            result = formatter.format(dt);
        } else if (flag == 2) {
            Calendar c = Calendar.getInstance(Locale.ENGLISH);
            c.add(Calendar.DATE, 2);
            Date dt = c.getTime();
            SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_YYYY_MM_DD_FORMAT);
            result = formatter.format(dt);
        } else if (flag == 3) {
            Calendar c = Calendar.getInstance(Locale.ENGLISH);
            c.add(Calendar.DATE, 3);
            Date dt = c.getTime();
            SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_YYYY_MM_DD_FORMAT);
            result = formatter.format(dt);
        }

        return result;
    }

    // -------------------------------------------------//
    // --------------Compare Date----------------------//
    // -------------------------------------------------//

    public static boolean DateIsBeforeThanProvidedDate(String date_first, String date_second, String dateformat) {
        boolean is_before = false;
        try {

            SimpleDateFormat sdf = new SimpleDateFormat(dateformat);
            Date date1 = sdf.parse(date_first);
            Date date2 = sdf.parse(date_second);

            System.out.println(sdf.format(date1));
            System.out.println(sdf.format(date2));

            if (date1.compareTo(date2) > 0) {
                is_before = true;
            }


        } catch (ParseException ex) {
            ex.printStackTrace();
        }

        return is_before;
    }
    // -------------------------------------------------//
    // --------------Compare Date----------------------//
    // -------------------------------------------------//

    public static boolean DateIsBeforeStrickThanProvidedDate(String date_first, String date_second, String dateformat) {
        boolean is_before = false;
        try {

            SimpleDateFormat sdf = new SimpleDateFormat(dateformat);
            Date date1 = sdf.parse(date_first);
            Date date2 = sdf.parse(date_second);

            System.out.println(sdf.format(date1));
            System.out.println(sdf.format(date2));

            if (date1.compareTo(date2) >= 0) {
                is_before = true;
            }


        } catch (ParseException ex) {
            ex.printStackTrace();
        }

        return is_before;
    }
    // -------------------------------------------------//
    // --------------Compare Date----------------------//
    // -------------------------------------------------//

    public static boolean DateIsBeforeThanOrEqualProvidedDate(String date_first, String date_second, String dateformat) {
        boolean is_before = false;
        try {

            SimpleDateFormat sdf = new SimpleDateFormat(dateformat);
            Date date1 = sdf.parse(date_first);
            Date date2 = sdf.parse(date_second);

            System.out.println(sdf.format(date1));
            System.out.println(sdf.format(date2));

            if (date1.compareTo(date2) == 1) {
                is_before = true;
            }


        } catch (ParseException ex) {
            ex.printStackTrace();
        }

        return is_before;
    }

    // -------------------------------------------------//
    // --------------check is equal Date----------------------//
    // -------------------------------------------------//

    public static boolean IsEqualDate(String date_first, String date_second, String dateformat) {
        boolean is_equal = false;
        try {

            SimpleDateFormat sdf = new SimpleDateFormat(dateformat);
            Date date1 = sdf.parse(date_first);
            Date date2 = sdf.parse(date_second);

            System.out.println(sdf.format(date1));
            System.out.println(sdf.format(date2));

            if (date1.compareTo(date2) == 0) {
                is_equal = true;
            }


        } catch (ParseException ex) {
            ex.printStackTrace();
        }

        return is_equal;
    }

    // -------------------------------------------------//
    // --------------Hide Keyboard----------------------//
    // -------------------------------------------------//

    public static void hideSoftKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) view.getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    // -------------------------------------------------//
    // --------------Show Keyboard----------------------//
    // -------------------------------------------------//

    public static void showSoftkeyboard(View view) {
        showSoftkeyboard(view, null);
    }

    public static void showSoftkeyboard(View view, ResultReceiver resultReceiver) {
        Configuration config = view.getContext().getResources()
                .getConfiguration();
        if (config.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_YES) {
            InputMethodManager imm = (InputMethodManager) view.getContext()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);

            if (resultReceiver != null) {
                imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT,
                        resultReceiver);
            } else {
                imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
            }
        }
    }

    // -------------------------------------------------//
    // --------------Open WebBrowser--------------------//
    // -------------------------------------------------//

    public static void startWebActivity(Context context, String url) {
        final Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        context.startActivity(intent);
    }

    // -------------------------------------------------//
    // --------------Open search in WebBrowser--------------------//
    // -------------------------------------------------//

    public static void startWebSearchActivity(Context context, String url) {
        final Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
        intent.putExtra(SearchManager.QUERY, url);
        context.startActivity(intent);
    }

    // -------------------------------------------------//
    // --------------Open Email Intent------------------//
    // -------------------------------------------------//

    public static void startEmailActivity(Context context, int toResId,
                                          int subjectResId, int bodyResId) {
        startEmailActivity(context, context.getString(toResId),
                context.getString(subjectResId), context.getString(bodyResId));
    }

    public static void startEmailActivity(Context context, String to,
                                          String subject, String body) {
        final Intent intent = new Intent(Intent.ACTION_SEND);
        //intent.setType("message/rfc822");
        intent.setType("text/html");
        if (!TextUtils.isEmpty(to)) {
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{to});
        }
        if (!TextUtils.isEmpty(subject)) {
            intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        }
        if (!TextUtils.isEmpty(body)) {
            intent.putExtra(Intent.EXTRA_TEXT, body);
        }

        final PackageManager pm = (PackageManager) context.getPackageManager();
        try {
            if (pm.queryIntentActivities(intent,
                    PackageManager.MATCH_DEFAULT_ONLY).size() == 0) {
                intent.setType("text/plain");
            }
        } catch (Exception e) {
            Log.w("Error.", e);
        }

        context.startActivity(intent);
    }

    // -------------------------------------------------//
    // --------------get app Name-----------------------//
    // -------------------------------------------------//

    public static String getAppName() {
        return getAppName(ctx, null);
    }

    public static String getAppName(Context context, String packageName) {
        String applicationName;

        if (packageName == null) {
            packageName = context.getPackageName();
        }

        try {
            PackageManager packageManager = context.getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(
                    packageName, 0);
            applicationName = context
                    .getString(packageInfo.applicationInfo.labelRes);
        } catch (Exception e) {
            Log.w("error", "Failed to get version number." + e);
            applicationName = "";
        }

        return applicationName;
    }

    // -------------------------------------------------//
    // --------------get app VersionNumber--------------//
    // -------------------------------------------------//
    public static String getAppVersionNumber() {
        return getAppVersionNumber(ctx, null);
    }

    public static String getAppVersionNumber(Context context, String packageName) {
        String versionName;

        if (packageName == null) {
            packageName = context.getPackageName();
        }

        try {
            PackageManager packageManager = context.getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(
                    packageName, 0);
            versionName = packageInfo.versionName;
        } catch (Exception e) {
            // Log.e("Failed to get version number.",""+e);
            e.printStackTrace();
            versionName = "";
        }

        return versionName;
    }

    // -------------------------------------------------//
    // --------------get app VersionCode----------------//
    // -------------------------------------------------//

    public static String getAppVersionCode() {
        return getAppVersionCode(ctx, null);
    }

    public static String getAppVersionCode(Context context, String packageName) {
        String versionCode;

        if (packageName == null) {
            packageName = context.getPackageName();
        }

        try {
            PackageManager packageManager = context.getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(
                    packageName, 0);
            versionCode = Integer.toString(packageInfo.versionCode);
        } catch (Exception e) {
            Log.w("Failed ", e);
            versionCode = "";
        }

        return versionCode;
    }

    // -------------------------------------------------//
    // --------------get SdkVersion---------------------//
    // -------------------------------------------------//
    public static int getSdkVersion() {
        try {
            return Build.VERSION.class.getField("SDK_INT").getInt(null);
        } catch (Exception e) {
            return 3;
        }
    }

    // -------------------------------------------------//
    // --------check is app run on emulator-------------//
    // -------------------------------------------------//
    public static boolean isEmulator() {
        return Build.MODEL.equals("sdk") || Build.MODEL.equals("google_sdk");
    }

    // -------------------------------------------------//
    // --------convert dp to pixcel---------------------//
    // -------------------------------------------------//

    public static float dpToPx(float dp) {
        if (ctx == null) {
            return -1;
        }
        return dp * ctx.getResources().getDisplayMetrics().density;
    }

    // -------------------------------------------------//
    // --------convert pixcel to dp --------------------//
    // -------------------------------------------------//
    public static float pxToDp(float px) {
        if (ctx == null) {
            return -1;
        }
        return px / ctx.getResources().getDisplayMetrics().density;
    }

    // -------------------------------------------------//
    // --------convert dp to pixcel integer-------------//
    // -------------------------------------------------//
    public static float dpToPxInt(Context context, float dp) {
        return (int) (dpToPx(dp) + 0.5f);
    }

    // -------------------------------------------------//
    // --------convert pixcel to DpCeilInt--------------//
    // -------------------------------------------------//
    public static float pxToDpCeilInt(float px) {
        return (int) (pxToDp(px) + 0.5f);
    }

    // -------------------------------------------------//
    // --------geFileFromAssets-------------------------//
    // -------------------------------------------------//

    public static String geFileFromAssets(String fileName) {
        if (ctx == null || isEmpty(fileName)) {
            return null;
        }

        StringBuilder s = new StringBuilder("");
        try {
            InputStreamReader in = new InputStreamReader(ctx.getResources()
                    .getAssets().open(fileName));
            BufferedReader br = new BufferedReader(in);
            String line;
            while ((line = br.readLine()) != null) {
                s.append(line);
            }
            return s.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean isEmpty(CharSequence str) {
        return (str == null || str.length() == 0);
    }

    // -------------------------------------------------//
    // --------geFileFromRaw-------------------------//
    // -------------------------------------------------//

    public static String geFileFromRaw(int resId) {
        if (ctx == null) {
            return null;
        }

        StringBuilder s = new StringBuilder();
        try {
            InputStreamReader in = new InputStreamReader(ctx.getResources()
                    .openRawResource(resId));
            BufferedReader br = new BufferedReader(in);
            String line;
            while ((line = br.readLine()) != null) {
                s.append(line);
            }
            return s.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    // -------------------------------------------------//
    // --------get list of file from asset--------------//
    // -------------------------------------------------//

    public static List<String> geFileToListFromAssets(String fileName) {
        if (ctx == null || isEmpty(fileName)) {
            return null;
        }

        List<String> fileContent = new ArrayList<String>();
        try {
            InputStreamReader in = new InputStreamReader(ctx.getResources()
                    .getAssets().open(fileName));
            BufferedReader br = new BufferedReader(in);
            String line;
            while ((line = br.readLine()) != null) {
                fileContent.add(line);
            }
            br.close();
            return fileContent;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    // -------------------------------------------------//
    // --------get list of file from raw--------------//
    // -------------------------------------------------//
    public static List<String> geFileToListFromRaw(int resId) {
        if (ctx == null) {
            return null;
        }

        List<String> fileContent = new ArrayList<String>();
        BufferedReader reader = null;
        try {
            InputStreamReader in = new InputStreamReader(ctx.getResources()
                    .openRawResource(resId));
            reader = new BufferedReader(in);
            String line = null;
            while ((line = reader.readLine()) != null) {
                fileContent.add(line);
            }
            reader.close();
            return fileContent;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    // -------------------------------------------------//
    // ---get random number and letter combination------//
    // -------------------------------------------------//

    public static String getRandomNumbersAndLetters(int length) {
        return getRandom(NUMBERS_AND_LETTERS, length);
    }

    // -------------------------------------------------//
    // --------------get random number only-------------//
    // -------------------------------------------------//
    public static String getRandomNumbers(int length) {
        return getRandom(NUMBERS, length);
    }

    // -------------------------------------------------//
    // --------------get random letter only-------------//
    // -------------------------------------------------//
    public static String getRandomLetters(int length) {
        return getRandom(LETTERS, length);
    }

    // -------------------------------------------------//
    // --------------get random capital letter----------//
    // -------------------------------------------------//

    public static String getRandomCapitalLetters(int length) {
        return getRandom(CAPITAL_LETTERS, length);
    }

    // -------------------------------------------------//
    // --------------get random lowecase letter----------//
    // -------------------------------------------------//

    public static String getRandomLowerCaseLetters(int length) {
        return getRandom(LOWER_CASE_LETTERS, length);
    }

    public static String getRandom(String source, int length) {
        return isEmpty(source) ? null : getRandom(source.toCharArray(), length);
    }

    public static String getRandom(char[] sourceChar, int length) {
        if (sourceChar == null || sourceChar.length == 0 || length < 0) {
            return null;
        }

        StringBuilder str = new StringBuilder(length);
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            str.append(sourceChar[random.nextInt(sourceChar.length)]);
        }
        return str.toString();
    }

    public static int getRandom(int max) {
        return getRandom(0, max);
    }

    public static int getRandom(int min, int max) {
        if (min > max) {
            return 0;
        }
        if (min == max) {
            return min;
        }
        return min + new Random().nextInt(max - min);
    }

    public static boolean shuffle(Object[] objArray) {
        if (objArray == null) {
            return false;
        }

        return shuffle(objArray, getRandom(objArray.length));
    }

    public static boolean shuffle(Object[] objArray, int shuffleCount) {
        int length;
        if (objArray == null || shuffleCount < 0
                || (length = objArray.length) < shuffleCount) {
            return false;
        }

        for (int i = 1; i <= shuffleCount; i++) {
            int random = getRandom(length - i);
            Object temp = objArray[length - i];
            objArray[length - i] = objArray[random];
            objArray[random] = temp;
        }
        return true;
    }

    public static int[] shuffle(int[] intArray) {
        if (intArray == null) {
            return null;
        }

        return shuffle(intArray, getRandom(intArray.length));
    }

    public static int[] shuffle(int[] intArray, int shuffleCount) {
        int length;
        if (intArray == null || shuffleCount < 0
                || (length = intArray.length) < shuffleCount) {
            return null;
        }

        int[] out = new int[shuffleCount];
        for (int i = 1; i <= shuffleCount; i++) {
            int random = getRandom(length - i);
            out[i - 1] = intArray[random];
            int temp = intArray[length - i];
            intArray[length - i] = intArray[random];
            intArray[random] = temp;
        }
        return out;
    }

    // -------------------------------------------------//
    // --------------encode string with MD5-------------//
    // -------------------------------------------------//

    public static String MD5(String str) {
        if (str == null) {
            return null;
        }
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(str.getBytes());
            return new String(encodeHex(messageDigest.digest()));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    protected static char[] encodeHex(final byte[] data) {
        final int l = data.length;
        final char[] out = new char[l << 1];
        // two characters form the hex value.
        for (int i = 0, j = 0; i < l; i++) {
            out[j++] = DIGITS_LOWER[(0xF0 & data[i]) >>> 4];
            out[j++] = DIGITS_LOWER[0x0F & data[i]];
        }
        return out;
    }

    // -------------------------------------------------//
    // -----------isApplicationInBackground-------------//
    // -------------------------------------------------//

    public static boolean isApplicationInBackground() {
        ActivityManager am = (ActivityManager) ctx
                .getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningTaskInfo> taskList = am.getRunningTasks(1);
        if (taskList != null && !taskList.isEmpty()) {
            ComponentName topActivity = taskList.get(0).topActivity;
            if (topActivity != null
                    && !topActivity.getPackageName().equals(
                    ctx.getPackageName())) {
                return true;
            }
        }
        return false;
    }

    // -------------------------------------------------//
    // -----------compressImage--------------------------//
    // -------------------------------------------------//
    public Bitmap compressImage(String imagepath) {

        String filePath = imagepath;
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

        // by setting this field as true, the actual bitmap pixels are not
        // loaded in the memory. Just the bounds are loaded. If
        // you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

        // max Height and width values of the compressed image is taken as
        // 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

        // width and height values are set maintaining the aspect ratio of the
        // image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        // setting inSampleSize value allows to load a scaled down version of
        // the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth,
                actualHeight);

        // inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

        // this options allow android to claim the bitmap memory if it runs low
        // on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            // load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight,
                    Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2,
                middleY - bmp.getHeight() / 2, new Paint(
                        Paint.FILTER_BITMAP_FLAG));

        // check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = imagepath;
        try {
            out = new FileOutputStream(filename);

            // write the compressed bitmap at the destination specified by
            // filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return scaledBitmap;

    }

    public int calculateInSampleSize(BitmapFactory.Options options,
                                     int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height
                    / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = ctx.getContentResolver().query(contentUri, null, null,
                null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor
                    .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public Bitmap RotateImageAfterPick(String sourcepath) {
        int rotate = 0;
        Bitmap mbitmap = compressImage(sourcepath);
        try {
            File imageFile = new File(sourcepath);
            ;
            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Matrix matrix = new Matrix();
        matrix.postRotate(rotate);
        Bitmap bitmap = Bitmap.createBitmap(mbitmap, 0, 0, mbitmap.getWidth(),
                mbitmap.getHeight(), matrix, true);
        return bitmap;
    }

    // -------------------------------------------------//
    // -----------Round Double Value--------------------//
    // -------------------------------------------------//
    public static String round(double value, int places) {
        double a = value;
        BigDecimal bd = new BigDecimal(a);
        BigDecimal res = bd.setScale(places, RoundingMode.DOWN);
        System.out.println("" + res.toPlainString());
        return res.toPlainString();
    }

    // -------------------------------------------------//
    // -----------DoubleToInt--------------------//
    // -------------------------------------------------//
    public static String DoubleToInt(String value) {
        double a = Double.valueOf(value);
        BigDecimal bd = new BigDecimal(a);

        System.out.println("" + bd.intValue());
        return "" + bd.intValue();
    }

    // -------------------------------------------------//
    // -----------GetPriceInFormat--------------------//
    // -------------------------------------------------//
    public static String ConvertPriceInFormat(String value) {
        double a = Double.valueOf(value);
        BigDecimal bd = new BigDecimal(a);

        System.out.println("" + bd.intValue());
        return "$" + bd.intValue();
    }

    // -------------------------------------------------//
    // -----------Show Custome Dialog-------------------//
    // -------------------------------------------------//
    public static void ShowAlert(Context ctx, String msg, String title) {
        try {


            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    ctx);

            // set title
            alertDialogBuilder.setTitle("" + title);

            // set dialog message
            alertDialogBuilder
                    .setMessage("" + msg)
                    .setCancelable(false)
                    .setPositiveButton(ctx.getResources().getString(R.string.text_ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            dialog.cancel();
                        }
                    });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // -------------------------------------------------//
    // -----------GetDeviceID---------------------------//
    // -------------------------------------------------//

    public static String GetDeviceID() {
        // TODO Auto-generated method stub
        String id = Settings.Secure.getString(ctx.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        return id;

    }

    // -------------------------------------------------//
    // -----------isActivityRunning---------------------//
    // -------------------------------------------------//


    public static boolean isActivityRunning(Context context, String actvity_name) {

        boolean isActivityFound;

        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningTaskInfo> activitys = activityManager.getRunningTasks(Integer.MAX_VALUE);
        isActivityFound = false;
        for (int i = 0; i < activitys.size(); i++) {
            if (activitys.get(i).topActivity.toString().equalsIgnoreCase("ComponentInfo{com.sikkaa/com.sikkaa." + actvity_name + "}")) {
                isActivityFound = true;
            }
        }
        return isActivityFound;
    }

    // -------------------------------------------------//
    // -----------isAppInForeground---------------------//
    // -------------------------------------------------//
    public static boolean isForeground(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(context.ACTIVITY_SERVICE);
        List<RunningTaskInfo> runningTaskInfo = am.getRunningTasks(1);

        ComponentName componentInfo = runningTaskInfo.get(0).topActivity;
        if (componentInfo.getPackageName().equals("com.sikkaa")) return true;
        return false;
    }
// -------------------------------------------------//
    // -----------setTotalHeightofListView---------------------//
    // -------------------------------------------------//

    public static void setTotalHeightofListView(ListView listView, Context ctx) {

        ListAdapter mAdapter = listView.getAdapter();

        int totalHeight = 0;

        for (int i = 0; i < mAdapter.getCount(); i++) {
            View mView = mAdapter.getView(i, null, listView);

            mView.measure(
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),

                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));

            totalHeight += mView.getMeasuredHeight();
            Log.w("HEIGHT" + i, String.valueOf(totalHeight));

        }


        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight
                + (listView.getDividerHeight() * (mAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();


    }


    // -------------------------------------------------//
    // -----------Clear All Preferences-----------------//
    // -------------------------------------------------//

    public static void ClearAllPreferences() {
        SharedPreferences prefs = ctx.getSharedPreferences(PREFERENCE_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.commit();

    }

    // -------------------------------------------------//
    // -----------create images JSONArray-----------------//
    // -------------------------------------------------//

//    public static String createJSONArrayFromArrayList(ArrayList<HashMap<String, String>> img_array) {
//        JSONArray jsonImages = new JSONArray();
//        for (int i = 0; i < img_array.size(); i++) {
//            JSONObject json = new JSONObject();
//            try {
//                if (img_array.get(i).get(Constants.is_from_server).equals(Constants.SERVER_IMAGE)) {
//                    DataMyAdsListImages dataJobListImages = new GsonBuilder().create().fromJson(img_array.get(i).get(Constants.data), DataMyAdsListImages.class);
//                    json.put(Constants.property_id, "" + dataJobListImages.getPropertyId());
//                    json.put(Constants.property_image_id, "" + dataJobListImages.getPropertyImageId());
//                    json.put(Constants.is_del, "" + dataJobListImages.getIsDel());
//
//                    if (dataJobListImages.getIsDel().equals("" + Constants.IS_DEL))
//                        jsonImages.put(json);
//
//                } else {
//                    json.put(Constants.property_image_id, "");
//                    json.put(Constants.property_id, "");
//                    json.put(Constants.property_photo, "" + img_array.get(i).get(Constants.img_name));
//                    json.put(Constants.is_del, "0");
//                    jsonImages.put(json);
//                }
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//        return (jsonImages.length() > 0) ? jsonImages.toString() : "";
//
//    }


    public static String createJSONArrayFromProductList(ArrayList<HashMap<String, String>> img_array) {
        JSONArray jsonImages = new JSONArray();
        for (int i = 0; i < img_array.size(); i++) {
            JSONObject json = new JSONObject();
            try {
                json.put(Constants.product_id, "" + img_array.get(i).get(Constants.product_id));
                json.put(Constants.category_id, "" + img_array.get(i).get(Constants.category_id));
                json.put(Constants.quantity, "" + img_array.get(i).get(Constants.quantity));
                json.put(Constants.notes, "" + img_array.get(i).get(Constants.notes));
                jsonImages.put(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return (jsonImages.length() > 0) ? jsonImages.toString() : "";

    }

    public static String createJSONArrayFromArrayList(ArrayList<HashMap<String, String>> img_array) {
        JSONArray jsonImages = new JSONArray();
        for (int i = 0; i < img_array.size(); i++) {
            JSONObject json = new JSONObject();
            try {
                json.put(Constants.img_name, "" + img_array.get(i).get(Constants.img_name));
                jsonImages.put(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return (jsonImages.length() > 0) ? jsonImages.toString() : "";

    }
    public static String createJSONArrayFromArrayListForUpdate(ArrayList<HashMap<String, String>> img_array) {
        JSONArray jsonImages = new JSONArray();
        for (int i = 0; i < img_array.size(); i++) {
            JSONObject json = new JSONObject();
            try {
                if (("" + img_array.get(i).get(Constants.is_from_server)).equalsIgnoreCase(Constants.LOCAL_IMAGE)) {
                    json.put(Constants.img_name, img_array.get(i).get(Constants.img_name));
                    json.put(Constants.image_id, "0");
                    json.put(Constants.is_del, "0");
                    jsonImages.put(json);
                } else {
                    if (("" + img_array.get(i).get(Constants.is_edit)).equalsIgnoreCase("1") ||
                            ("" + img_array.get(i).get(Constants.is_del)).equalsIgnoreCase("1")) {
                        json.put(Constants.img_name, img_array.get(i).get(Constants.img_name));
                        json.put(Constants.image_id, img_array.get(i).get(Constants.img_id));
                        json.put(Constants.is_del, img_array.get(i).get(Constants.is_del));
                        jsonImages.put(json);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return (jsonImages.length() > 0) ? jsonImages.toString() : "";

    }


    // -------------------------------------------------//
    // -----------GetDistance------------//
    // -------------------------------------------------//
    public static double GetDistance(double lat1, double lon1, double lat2,
                                     double lon2) {
        // TODO Auto-generated method stub
        android.location.Location locationA = new android.location.Location("a");
        locationA.setLatitude(lat1);
        locationA.setLongitude(lon1);
        android.location.Location LocationB = new android.location.Location("b");
        LocationB.setLatitude(lat2);
        LocationB.setLongitude(lon2);
        // return in mi
        // return locationA.distanceTo(LocationB) * 0.000621371;
        // ---------------------
        // return in mitter
        return locationA.distanceTo(LocationB);
    }

    public static double RoundDouble(double value, int places) {
        if (places < 0)
            throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }


    // -------------------------------------------------//
    // -----------appInstalledOrNot---------------------//
    // -------------------------------------------------//

    public static boolean ISappInstalledOrNot(String uri) {
        PackageManager pm = ctx.getPackageManager();
        boolean app_installed = false;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

    // -------------------------------------------------//
    // -----------GetCurrentTimeStamp---------------------//
    // -------------------------------------------------//

    public static String GetCurrentTimeStamp() {
        Long tsLong = System.currentTimeMillis();
        String ts = tsLong.toString();
        return ts;
    }


    // -------------------------------------------------//
    // -----------GetDefaultTimeZone---------------------//
    // -------------------------------------------------//

    public static String GetDefaultTimeZone() {
        TimeZone tz = TimeZone.getDefault();
        try {
            return "" + tz.getID();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

    }

    // -------------------------------------------------//
    // -----------GetCurrentNanoTime---------------------//
    // -------------------------------------------------//

    public static String GetCurrentNanoTime() {
        Long tsLong = System.nanoTime();
        String ts = tsLong.toString();
        return ts;
    }

    // -------------------------------------------------//
    // ------------ GetDateFromTimeStamp----------------------//
    // -------------------------------------------------//

    public static String GetDateFromTimeStamp(String milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.valueOf(milliSeconds));
        return formatter.format(calendar.getTime());
    }

    // -------------------------------------------------//
    // -----------roundDouble---------------------------//
    // -------------------------------------------------//

    public static double roundDouble(double Rval, int numberOfDigitsAfterDecimal) {
        double p = (float) Math.pow(10, numberOfDigitsAfterDecimal);
        Rval = Rval * p;
        double tmp = Math.floor(Rval);
        System.out.println("~~~~~~tmp~~~~~" + tmp);
        return (double) tmp / p;
    }


    public static void showProgressDialog(Context context) {
        pDialog = new ProgressDialog(context);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setCancelable(false);
        pDialog.setMessage(context.getResources().getString(R.string.txt_loading));
        pDialog.show();
    }

    public static void dismissProgressDialog() {
        if (pDialog != null) {

            if (pDialog.isShowing())
                pDialog.dismiss();
        }
    }

    public static void stopRefreshing(SwipeRefreshLayout mSwipeContainer) {

        if (mSwipeContainer != null) {
            if (mSwipeContainer.isRefreshing())
                mSwipeContainer.setRefreshing(false);
        }

    }

    public static boolean isRefreshing(SwipeRefreshLayout mSwipeContainer) {
        boolean isRefreshing = false;
        if (mSwipeContainer != null) {
            if (mSwipeContainer.isRefreshing())
                isRefreshing = true;
        }
        return isRefreshing;
    }


    public static void showSimillorProgressDialog(Context context) {
        pDialogSimilor = new ProgressDialog(context);
        pDialogSimilor.setCanceledOnTouchOutside(false);
        pDialogSimilor.setCancelable(false);
        pDialogSimilor.setMessage(context.getResources().getString(R.string.txt_loading));
        pDialogSimilor.show();
    }

    public static void dismissSimilorProgressDialog() {
        if (pDialogSimilor != null) {

            if (pDialogSimilor.isShowing())
                pDialogSimilor.dismiss();
        }
    }

    public static void showTournamentDialog(Context context) {
        pDialogTournament = new ProgressDialog(context);
        pDialogTournament.setCanceledOnTouchOutside(false);
        pDialogTournament.setCancelable(false);
        pDialogTournament.setMessage(context.getResources().getString(R.string.txt_loading));
        pDialogTournament.show();
    }

    public static void dismissTournamentDialog() {
        if (pDialogTournament != null) {

            if (pDialogTournament.isShowing())
                pDialogTournament.dismiss();
        }
    }
 public static void showSecondDialog(Context context) {
        pDialogSecond = new ProgressDialog(context);
     pDialogSecond.setCanceledOnTouchOutside(false);
     pDialogSecond.setCancelable(false);
     pDialogSecond.setMessage(context.getResources().getString(R.string.txt_loading));
     pDialogSecond.show();
    }

    public static void dismissSecondDialog() {
        if (pDialogSecond != null) {

            if (pDialogSecond.isShowing())
                pDialogSecond.dismiss();
        }
    }

    public static void StartRefreshing(SwipeRefreshLayout mSwipeContainer) {

        if (mSwipeContainer != null) {
            mSwipeContainer.setRefreshing(true);

        }
    }

    public static void StopRefreshing(SwipeRefreshLayout mSwipeContainer) {
        if (mSwipeContainer != null) {
            if (mSwipeContainer.isRefreshing()) {
                mSwipeContainer.setRefreshing(false);
            }
        }
    }


// -------------------------------------------------//
    //---------Open gogole map navigation activity---------//
    // -------------------------------------------------//

    public static void startGoogleMapNavigationActivity(Context context, double source_lat, double source_lng, double dest_lat, double dest_lng) {
        double lat2, lng2;


        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?" + "saddr=" + source_lat
                        + "," + source_lat + "&daddr=" + dest_lat + "," + dest_lng));
        intent.setClassName("com.google.android.apps.maps",
                "com.google.android.maps.MapsActivity");
        context.startActivity(intent);

    }


    // -------------------------------------------------//
    //---------Copy text to clipboard---------------//
    // -------------------------------------------------//

    public static boolean CopyTextToClipboard(String text) {
        try {
            int sdk = Build.VERSION.SDK_INT;
            if (sdk < Build.VERSION_CODES.HONEYCOMB) {
                android.text.ClipboardManager clipboard = (android.text.ClipboardManager) ctx.getSystemService(Context.CLIPBOARD_SERVICE);
                clipboard.setText(text);
            } else {
                android.content.ClipboardManager clipboard = (android.content.ClipboardManager) ctx.getSystemService(Context.CLIPBOARD_SERVICE);
                android.content.ClipData clip = android.content.ClipData.newPlainText(ctx.getResources().getString(R.string.app_name), text);
                clipboard.setPrimaryClip(clip);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean checkPermission(Context context, String permission) {
        return ActivityCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }


    /**
     * show Permissions Dialog
     *
     * @param activity
     * @param permissions
     * @param REQUEST_CODE
     */
    public static void showPermissionsDialog(Activity activity, String[] permissions, int REQUEST_CODE) {
        ActivityCompat.requestPermissions(activity, permissions, REQUEST_CODE);
    }


    /**
     * Make Native Call
     */
//    public static void makeNativeCall(Activity activity, String mobileNo) {
//        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mobileNo));
//        activity.startActivity(intent);
//    }

    // -------------------------------------------------//
    // -----------get getAddressFromLocation------------//
    // -------------------------------------------------//
    public static void getAddressFromLocation(final double latitude, final double longitude,
                                              final Context context, final Handler handler) {
        final String TAG = "getAddressFromLocation";
        Thread thread = new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                String result = null;
                Bundle bundle = new Bundle();
                try {
                    List<Address> addressList = geocoder.getFromLocation(
                            latitude, longitude, 1);
                    if (addressList != null && addressList.size() > 0) {
                        Address address = addressList.get(0);
                        StringBuilder sb = new StringBuilder();
                        for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {

                            if (i == 0) {
                                if (address.getAddressLine(0) != null) {
                                    if (!address.getAddressLine(0).equalsIgnoreCase("null")) {
                                        sb.append(address.getAddressLine(0)).append(" ");
                                    }
                                }
                                break;
                            }
                        }

                        if (address.getLocality() != null) {
                            if (!address.getLocality().equalsIgnoreCase("null")) {
                                //sb.append(address.getLocality()).append(" ");
                                bundle.putString(Constants.state, address.getLocality());
                            }
                        }
                        if (address.getPostalCode() != null) {
                            if (!address.getPostalCode().equalsIgnoreCase("null")) {
                                //sb.append(address.getPostalCode()).append(" ");
                                bundle.putString(Constants.postal_code, address.getPostalCode());
                            }
                        }

                        if (address.getCountryName() != null) {
                            if (!address.getCountryName().equalsIgnoreCase("null")) {
                                //sb.append(address.getCountryName()).append(" ");
                                bundle.putString(Constants.country, address.getAdminArea());
                            }
                        }

                        result = sb.toString();
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Unable connect to Geocoder", e);
                } finally {
                    Message message = Message.obtain();
                    message.setTarget(handler);
                    if (result != null) {
                        message.what = 1;

                        bundle.putString(Constants.addressline_1, result);

                        message.setData(bundle);
                    } else {
                        message.what = 1;
                        bundle.putString(Constants.addressline_1, "");
                        bundle.putString(Constants.postal_code, "");
                        bundle.putString(Constants.country, "");
                        bundle.putString(Constants.state, "");


                        message.setData(bundle);
                    }
                    message.sendToTarget();
                }
            }
        };
        thread.start();

    }


}
