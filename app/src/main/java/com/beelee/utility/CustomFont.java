package com.beelee.utility;

import android.content.Context;
import android.graphics.Typeface;

public class CustomFont {
    public Typeface font_bold, font_medium, font_light, font_regular;

    public CustomFont(Context context) {

        font_regular = Typeface.createFromAsset(context.getAssets(), "Gotham_Book.otf");
        font_bold = Typeface.createFromAsset(context.getAssets(), "Gotham_Bold.otf");
        font_light = Typeface.createFromAsset(context.getAssets(), "Gotham_Book.otf");
        font_medium = Typeface.createFromAsset(context.getAssets(), "Gotham_Book.otf");
    }


}
